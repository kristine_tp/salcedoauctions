package com.yondu.salcedoauctions.api;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.yondu.base.utils.DebugLog;
import com.yondu.base.utils.SingleHttpRequestTask;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Banner;
import com.yondu.salcedoauctions.model.Category;
import com.yondu.salcedoauctions.model.Lot;
import com.yondu.salcedoauctions.model.Page;
import com.yondu.salcedoauctions.model.SearchResult;
import com.yondu.salcedoauctions.model.User;
import com.yondu.salcedoauctions.model.Video;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class SalcedoAuctions implements SingleHttpRequestTask.OnHttpRequestListener {

  public static final String API_HOST = "http://52.74.182.111/";

  public static final String WEB_BASE = API_HOST + "dev/public/client/";
  public static final String WEB_REGISTER = WEB_BASE + "registration";

  public static final String IMAGE_BASE = API_HOST + "dev/public/images/uploaded/";
  public static final String IMAGE_CATEGORIES = IMAGE_BASE + "tags/";

  private static final String API_BASE = API_HOST + "api/v1/";
  private static final String API_LOGIN = "signin/";
  private static final String API_REGISTER = "signup/";
  private static final String API_FORGOT_PASSWORD = "forgot-password";
  private static final String API_INTERESTS = "interests/";
  private static final String API_INTERESTS_ADD = "interests/add";
  private static final String API_BANNERS = "banners/";
  private static final String API_AUCTIONS = "auctions/";
  private static final String API_AUCTIONS_UPCOMING = "upcoming";
  private static final String API_AUCTIONS_PAST = "past";
  private static final String API_LOTS = "lots/";
  private static final String API_FOLLOWS = "follow/";
  private static final String API_FOLLOW_ADD = "add";
  private static final String API_FOLLOW_REMOVE = "remove";
  private static final String API_FOLLOW_CHECK = "check";
  private static final String API_SEARCH = "search/";
  private static final String API_CATEGORIES = "categories/";
  private static final String API_CATEGORY_AUCTIONS = API_AUCTIONS + "cat/";
  private static final String API_VIDEOS = "videos/";
  private static final String API_PAGE = "pages/";

  public static final String PAGE_ABOUT = "about-us";
  public static final String PAGE_CONTACT = "contact-us";
  public static final String PAGE_FAQ = "faq";
  public static final String PAGE_TERMS = "terms-and-conditions";

  public static final int REQUEST_LOGIN = 1001;
  public static final int REQUEST_REGISTER = 1002;
  public static final int REQUEST_FORGOT_PASSWORD = 1003;
  public static final int REQUEST_INTERESTS = 1004;
  public static final int REQUEST_INTERESTS_ADD = 1005;
  public static final int REQUEST_BANNERS = 1006;
  public static final int REQUEST_AUCTIONS_UPCOMING = 1007;
  public static final int REQUEST_AUCTIONS_PAST = 1008;
  public static final int REQUEST_LOTS = 1009;
  public static final int REQUEST_FOLLOWS = 1010;
  public static final int REQUEST_FOLLOW_UPDATE = 1011;
  public static final int REQUEST_FOLLOW_CHECK = 1012;
  public static final int REQUEST_CALENDAR = 1013;
  public static final int REQUEST_SEARCH = 1014;
  public static final int REQUEST_CATEGORIES = 1015;
  public static final int REQUEST_CATEGORY_AUCTIONS = 1016;
  public static final int REQUEST_VIDEOS = 1017;
  public static final int REQUEST_PAGE = 1018;

  public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

  private Context mContext;
  private static SalcedoAuctions mInstance;

  private SingleHttpRequestTask mLoginRequest, mRegisterRequest, mForgotPasswordRequest, mGetInterestsRequest, mAddInterestsRequest, mGetBannersRequest, mGetAuctionsRequest, mGetCalendarRequest, mGetLotsRequest, mGetFollowsRequest,
      mFollowUpdateRequest, mFollowCheckRequest, mSearchRequest, mGetCategoriesRequest, mGetCategoryAuctionsRequest, mGetVideosRequest, mGetPageRequest;
  private OnLoginListener mOnLoginListener;
  private OnRegisterListener mOnRegisterListener;
  private OnGetInterestsListener mOnGetInterestsListener;
  private OnGetBannersListener mOnGetBannersListener;
  private OnGetAuctionsListener mOnGetAuctionsListener, mOnGetCalendarListener, mOnGetFollowsListener, mOnGetCategoryAuctionsListener;
  private OnGetLotsListener mOnGetLotsListener;
  private OnFollowUpdateListener mOnFollowUpdateListener;
  private OnFollowCheckListener mOnFollowCheckListener;
  private OnGetSearchResultsListener mOnGetSearchResultsListener;
  private OnGetCategoriesListener mOnGetCategoriesListener;
  private OnGetVideosListener mOnGetVideosListener;
  private OnGetPageListener mOnGetPageListener;

  private SalcedoAuctions(Context context) {
    mContext = context;
  }

  public static SalcedoAuctions getInstance(Context context) {
    if (mInstance == null) mInstance = new SalcedoAuctions(context);
    return mInstance;
  }

  public void cancelRequest(int requestCode) {
    SingleHttpRequestTask request = null;
    switch (requestCode) {
      case REQUEST_LOGIN:
        request = mLoginRequest;
        break;
    }
    if (request != null && request.isActive()) request.cancel(true);
  }

  public void login(String email, String password, WeakReference<Activity> weakActivity, OnLoginListener listener) {
    mOnLoginListener = listener;
    String uri = API_BASE + API_LOGIN;
    String vars = "eml=" + email;
    vars += "&psrd=" + password;

    if (mLoginRequest != null && mLoginRequest.isActive())
      mLoginRequest.cancel(true);
    mLoginRequest = new SingleHttpRequestTask(REQUEST_LOGIN, weakActivity, this);
    mLoginRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, vars, null));
  }

  public void register(String firstname, String lastname, String email, String password, WeakReference<Activity> weakActivity, OnRegisterListener listener) {
    mOnRegisterListener = listener;
    String uri = API_BASE + API_REGISTER;
    String vars = "fname=" + firstname;
    vars += "&lname=" + lastname;
    vars += "&email=" + email;
    vars += "&pass=" + password;

    if (mRegisterRequest != null && mRegisterRequest.isActive())
      mRegisterRequest.cancel(true);
    mRegisterRequest = new SingleHttpRequestTask(REQUEST_REGISTER, weakActivity, this);
    mRegisterRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, vars, null));
  }

  public void forgotPassword(String email, WeakReference<Activity> weakActivity, OnRegisterListener listener) {
    mOnRegisterListener = listener;
    String uri = API_BASE + API_FORGOT_PASSWORD;
    String vars = "f_email=" + email;

    if (mForgotPasswordRequest != null && mForgotPasswordRequest.isActive())
      mForgotPasswordRequest.cancel(true);
    mForgotPasswordRequest = new SingleHttpRequestTask(REQUEST_FORGOT_PASSWORD, weakActivity, this);
    mForgotPasswordRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, vars, null));
  }

  public void getInterests(int userId, WeakReference<Activity> weakActivity, OnGetInterestsListener listener) {
    mOnGetInterestsListener = listener;
    String uri = API_BASE + API_INTERESTS + userId;
    if (mGetInterestsRequest != null && mGetInterestsRequest.isActive())
      mGetInterestsRequest.cancel(true);
    mGetInterestsRequest = new SingleHttpRequestTask(REQUEST_INTERESTS, weakActivity, this);
    mGetInterestsRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void addInterests(int userId, List<Integer> interestCategoryIds, WeakReference<Activity> weakActivity, OnGetInterestsListener listener) {
    mOnGetInterestsListener = listener;
    String interestIds = "";
    for (Integer id : interestCategoryIds)
      interestIds += (id + ",");
    interestIds = interestIds.substring(0, interestIds.length() - 1);
    String uri = API_BASE + API_INTERESTS_ADD;
    String vars = "p=" + interestIds;
    vars += "&client_id=" + userId;
    DebugLog.d(this, vars);

    if (mAddInterestsRequest != null && mAddInterestsRequest.isActive())
      mAddInterestsRequest.cancel(true);
    mAddInterestsRequest = new SingleHttpRequestTask(REQUEST_INTERESTS_ADD, weakActivity, this);
    mAddInterestsRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, vars, null));
  }

  public void getBanners(WeakReference<Activity> weakActivity, OnGetBannersListener listener) {
    mOnGetBannersListener = listener;
    String uri = API_BASE + API_BANNERS;
    if (mGetBannersRequest != null && mGetBannersRequest.isActive())
      mGetBannersRequest.cancel(true);
    mGetBannersRequest = new SingleHttpRequestTask(REQUEST_BANNERS, weakActivity, this);
    mGetBannersRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getAuctions(Auction.Type auctionType, WeakReference<Activity> weakActivity, OnGetAuctionsListener listener) {
    mOnGetAuctionsListener = listener;
    String uri = API_BASE + API_AUCTIONS + (auctionType == Auction.Type.UPCOMING ? API_AUCTIONS_UPCOMING : API_AUCTIONS_PAST);

    if (mGetAuctionsRequest != null && mGetAuctionsRequest.isActive())
      mGetAuctionsRequest.cancel(true);
    mGetAuctionsRequest = new SingleHttpRequestTask(auctionType == Auction.Type.UPCOMING ? REQUEST_AUCTIONS_UPCOMING : REQUEST_AUCTIONS_PAST, weakActivity, this);
    mGetAuctionsRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getCalendar(int month, int year, WeakReference<Activity> weakActivity, OnGetAuctionsListener listener) {
    mOnGetCalendarListener = listener;
    String uri = API_BASE + API_AUCTIONS + year + "/" + month;

    if (mGetCalendarRequest != null && mGetCalendarRequest.isActive())
      mGetCalendarRequest.cancel(true);
    mGetCalendarRequest = new SingleHttpRequestTask(REQUEST_CALENDAR, weakActivity, this);
    mGetCalendarRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getLots(int auctionId, WeakReference<Activity> weakActivity, OnGetLotsListener listener) {
    mOnGetLotsListener = listener;
    String uri = API_BASE + API_LOTS + auctionId;

    if (mGetLotsRequest != null && mGetLotsRequest.isActive())
      mGetLotsRequest.cancel(true);
    mGetLotsRequest = new SingleHttpRequestTask(REQUEST_LOTS, weakActivity, this);
    mGetLotsRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getFollows(int userId, WeakReference<Activity> weakActivity, OnGetAuctionsListener listener) {
    mOnGetFollowsListener = listener;
    String uri = API_BASE + API_FOLLOWS + userId;

    if (mGetFollowsRequest != null && mGetFollowsRequest.isActive())
      mGetFollowsRequest.cancel(true);
    mGetFollowsRequest = new SingleHttpRequestTask(REQUEST_FOLLOWS, weakActivity, this);
    mGetFollowsRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void followUpdate(boolean isAdd, int userId, int auctionId, WeakReference<Activity> weakActivity, OnFollowUpdateListener listener) {
    mOnFollowUpdateListener = listener;
    String uri = API_BASE + API_FOLLOWS + (isAdd ? API_FOLLOW_ADD : API_FOLLOW_REMOVE);
    String vars = "p=" + auctionId + "," + userId;

    if (mFollowUpdateRequest != null && mFollowUpdateRequest.isActive())
      mFollowUpdateRequest.cancel(true);
    mFollowUpdateRequest = new SingleHttpRequestTask(REQUEST_FOLLOW_UPDATE, weakActivity, this);
    mFollowUpdateRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, vars, null));
  }

  public void followCheck(int userId, int auctionId, WeakReference<Activity> weakActivity, OnFollowCheckListener listener) {
    mOnFollowCheckListener = listener;
    String uri = API_BASE + API_FOLLOWS + API_FOLLOW_CHECK;
    String vars = "p=" + auctionId + "," + userId;

    if (mFollowCheckRequest != null && mFollowCheckRequest.isActive())
      mFollowCheckRequest.cancel(true);
    mFollowCheckRequest = new SingleHttpRequestTask(REQUEST_FOLLOW_CHECK, weakActivity, this);
    mFollowCheckRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, vars, null));
  }

  public void search(String query, WeakReference<Activity> weakActivity, OnGetSearchResultsListener listener) {
    mOnGetSearchResultsListener = listener;
    String uri = API_BASE + API_SEARCH + query;

    if (mSearchRequest != null && mSearchRequest.isActive())
      mSearchRequest.cancel(true);
    mSearchRequest = new SingleHttpRequestTask(REQUEST_SEARCH, weakActivity, this);
    mSearchRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getCategories(WeakReference<Activity> weakActivity, OnGetCategoriesListener listener) {
    mOnGetCategoriesListener = listener;
    String uri = API_BASE + API_CATEGORIES;

    if (mGetCategoriesRequest != null && mGetCategoriesRequest.isActive())
      mGetCategoriesRequest.cancel(true);
    mGetCategoriesRequest = new SingleHttpRequestTask(REQUEST_CATEGORIES, weakActivity, this);
    mGetCategoriesRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getCategoryAuctions(int categoryId, WeakReference<Activity> weakActivity, OnGetAuctionsListener listener) {
    mOnGetCategoryAuctionsListener = listener;
    String uri = API_BASE + API_CATEGORY_AUCTIONS + categoryId;

    if (mGetCategoryAuctionsRequest != null && mGetCategoryAuctionsRequest.isActive())
      mGetCategoryAuctionsRequest.cancel(true);
    mGetCategoryAuctionsRequest = new SingleHttpRequestTask(REQUEST_CATEGORY_AUCTIONS, weakActivity, this);
    mGetCategoryAuctionsRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getVideos(WeakReference<Activity> weakActivity, OnGetVideosListener listener) {
    mOnGetVideosListener = listener;
    String uri = API_BASE + API_VIDEOS;

    if (mGetVideosRequest != null && mGetVideosRequest.isActive())
      mGetVideosRequest.cancel(true);
    mGetVideosRequest = new SingleHttpRequestTask(REQUEST_VIDEOS, weakActivity, this);
    mGetVideosRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  public void getPage(String url, WeakReference<Activity> weakActivity, OnGetPageListener listener) {
    mOnGetPageListener = listener;
    String uri = API_BASE + API_PAGE + url;

    if (mGetPageRequest != null && mGetPageRequest.isActive())
      mGetPageRequest.cancel(true);
    mGetPageRequest = new SingleHttpRequestTask(REQUEST_PAGE, weakActivity, this);
    mGetPageRequest.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SingleHttpRequestTask.RequestParams(uri, null, null));
  }

  private void onLogin(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    String errorMsg = "Error logging in. Please try again in a bit.";
    User user = null;
    if (isSuccess) {
      try {
        JSONObject userJson = new JSONObject(response).getJSONArray("user").getJSONObject(0);
        int id = Integer.parseInt(userJson.getString("client_id"));
        String email = userJson.getString("client_email");
        String firstname = userJson.getString("client_first_name");
        String lastname = userJson.getString("client_last_name");
        String salutation = userJson.getString("client_salutation");
        user = new User(id, email, firstname, lastname, salutation);
      } catch (Exception e) {
        isSuccess = false;
        errorMsg = "Error logging in. You've entered an incorrect username or password or your account has not yet been approved.";
        e.printStackTrace();
      }
    }

    if (mOnLoginListener != null) {
      mOnLoginListener.onLogin(isSuccess, user, errorMsg);
      mOnLoginListener = null;
    }
  }

  private void onRegister(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    String message = "Registration error. Please try again in a bit.";
    User user = null;
    if (isSuccess) {
      try {
        JSONObject userJson = new JSONObject(response).getJSONArray("user").getJSONObject(0);
        int id = Integer.parseInt(userJson.getString("client_id"));
        String email = userJson.getString("client_email");
        String firstname = userJson.getString("client_first_name");
        String lastname = userJson.getString("client_last_name");
        String salutation = userJson.getString("client_salutation");
        user = new User(id, email, firstname, lastname, salutation);
        message = "Registration successful. Please check your email for confirmation.";
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnRegisterListener != null) {
      mOnRegisterListener.onRegister(isSuccess, message);
      mOnRegisterListener = null;
    }
  }

  private void onForgotPassword(boolean isSuccess, String response) {
    DebugLog.v(this, response);
//    String message = "Registration error. Please try again in a bit.";
//    User user = null;
//    if (isSuccess) {
//      try {
//        JSONObject userJson = new JSONObject(response).getJSONArray("user").getJSONObject(0);
//        int id = Integer.parseInt(userJson.getString("client_id"));
//        String email = userJson.getString("client_email");
//        String firstname = userJson.getString("client_first_name");
//        String lastname = userJson.getString("client_last_name");
//        String salutation = userJson.getString("client_salutation");
//        user = new User(id, email, firstname, lastname, salutation);
//        message = "Registration successful. Please check your email for confirmation.";
//      } catch (Exception e) {
//        isSuccess = false;
//        e.printStackTrace();
//      }
//    }
//
//    if (mOnRegisterListener != null) {
//      mOnRegisterListener.onRegister(isSuccess, message);
//      mOnRegisterListener = null;
//    }
  }

  private void onGetInterests(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    boolean isInitiated = false;
    if (isSuccess) {
      try {
        JSONArray interests = new JSONObject(response).getJSONArray("interests");
        if (interests.length() > 0) isInitiated = true;
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnGetInterestsListener != null) {
      mOnGetInterestsListener.onGetInterests(isSuccess, isInitiated);
      mOnGetInterestsListener = null;
    }
  }

  private void onGetBanners(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    List<Banner> bannerList = new ArrayList<Banner>();
    if (isSuccess) {
      try {
        JSONArray banners = new JSONObject(response).getJSONArray("banners");
        for (int i = 0; i < banners.length(); i++)
          bannerList.add(new Banner(banners.getJSONObject(i)));
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnGetBannersListener != null) {
      mOnGetBannersListener.onGetBanners(isSuccess, bannerList);
      mOnGetBannersListener = null;
    }
  }

  private void onGetAuctions(int requestCode, boolean isSuccess, String response) {
    DebugLog.v(this, response);
    List<Auction> auctionList = new ArrayList<Auction>();
    if (isSuccess) {
      try {
        JSONArray auctions = new JSONObject(response).getJSONArray("auctions");
        for (int i = 0; i < auctions.length(); i++)
          auctionList.add(new Auction(auctions.getJSONObject(i)));
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    switch (requestCode) {
      case REQUEST_AUCTIONS_UPCOMING:
        if (mOnGetAuctionsListener != null)
          mOnGetAuctionsListener.onGetAuctions(isSuccess, Auction.Type.UPCOMING, auctionList);
        break;
      case REQUEST_AUCTIONS_PAST:
        if (mOnGetAuctionsListener != null)
          mOnGetAuctionsListener.onGetAuctions(isSuccess, Auction.Type.PAST, auctionList);
        mOnGetAuctionsListener = null;
        break;
      case REQUEST_CALENDAR:
        if (mOnGetCalendarListener != null)
          mOnGetCalendarListener.onGetAuctions(isSuccess, null, auctionList);
        mOnGetCalendarListener = null;
        break;
      case REQUEST_FOLLOWS:
        if (mOnGetFollowsListener != null)
          mOnGetFollowsListener.onGetAuctions(isSuccess, null, auctionList);
        mOnGetFollowsListener = null;
        break;
      case REQUEST_CATEGORY_AUCTIONS:
        if (mOnGetCategoryAuctionsListener != null)
          mOnGetCategoryAuctionsListener.onGetAuctions(isSuccess, null, auctionList);
        mOnGetCategoryAuctionsListener = null;
        break;
    }
  }

  private void onGetLots(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    List<Lot> lotList = new ArrayList<Lot>();
    if (isSuccess) {
      try {
        JSONArray lots = new JSONObject(response).getJSONArray("lots");
        for (int i = 0; i < lots.length(); i++)
          lotList.add(new Lot(lots.getJSONObject(i)));
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnGetLotsListener != null) {
      mOnGetLotsListener.onGetLots(isSuccess, lotList);
      mOnGetLotsListener = null;
    }
  }

  private void onFollowUpdate(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    if (isSuccess && response.toLowerCase().contains("success")) isSuccess = true;
    else isSuccess = false;

    if (mOnFollowUpdateListener != null) {
      mOnFollowUpdateListener.onFollowUpdate(isSuccess);
      mOnFollowUpdateListener = null;
    }
  }

  private void onFollowCheck(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    boolean isFollowed = false;
    if (isSuccess && response.toLowerCase().contains("true")) isFollowed = true;

    if (mOnFollowCheckListener != null) {
      mOnFollowCheckListener.onFollowCheck(isSuccess, isFollowed);
      mOnFollowCheckListener = null;
    }
  }

  private void onGetSearchResults(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    List<SearchResult> searchResultList = new ArrayList<SearchResult>();
    int count = 0;
    if (isSuccess) {
      try {
        JSONObject json = new JSONObject(response);
        if (json.has("auctions")) {
          JSONArray auctions = json.getJSONArray("auctions");
          if (auctions.length() > 0) {
            searchResultList.add(new SearchResult("Auctions"));
            count += auctions.length();
            DebugLog.d(this, "auctions: " + auctions.length());
            for (int i = 0; i < auctions.length(); i++) {
              searchResultList.add(new SearchResult(new Auction(auctions.getJSONObject(i))));
            }
            DebugLog.d(this, "search: " + searchResultList.size());
          }
        }
        if (json.has("lots")) {
          JSONArray lots = json.getJSONArray("lots");
          if (lots.length() > 0) {
            searchResultList.add(new SearchResult("Lots"));
            count += lots.length();
            for (int i = 0; i < lots.length(); i++)
              searchResultList.add(new SearchResult(new Lot(lots.getJSONObject(i))));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    if (mOnGetSearchResultsListener != null) {
      mOnGetSearchResultsListener.onGetSearchResults(isSuccess, count, searchResultList);
      mOnGetSearchResultsListener = null;
    }
  }

  private void onGetCategories(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    List<Category> categoryList = new ArrayList<Category>();
    if (isSuccess) {
      try {
        JSONArray categories = new JSONObject(response).getJSONArray("categories");
        for (int i = 0; i < categories.length(); i++)
          categoryList.add(new Category(categories.getJSONObject(i)));
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnGetCategoriesListener != null)
      mOnGetCategoriesListener.onGetCategories(isSuccess, categoryList);
    mOnGetCategoriesListener = null;
  }

  private void onGetVideos(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    List<Video> videoList = new ArrayList<Video>();
    if (isSuccess) {
      try {
        JSONArray videos = new JSONObject(response).getJSONArray("videos");
        for (int i = 0; i < videos.length(); i++)
          videoList.add(new Video(videos.getJSONObject(i)));
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnGetVideosListener != null) {
      mOnGetVideosListener.onGetVideos(isSuccess, videoList);
      mOnGetVideosListener = null;
    }
  }

  private void onGetPage(boolean isSuccess, String response) {
    DebugLog.v(this, response);
    Page page = null;
    if (isSuccess) {
      try {
        JSONArray pages = new JSONObject(response).getJSONArray("pages");
        if (pages.length() > 0) page = new Page(pages.getJSONObject(0));
      } catch (Exception e) {
        isSuccess = false;
        e.printStackTrace();
      }
    }

    if (mOnGetPageListener != null) {
      mOnGetPageListener.onGetPage(isSuccess, page);
      mOnGetPageListener = null;
    }
  }

  @Override
  public void onHttpResult(SingleHttpRequestTask request, int requestCode, boolean isSuccess, String response) {
    DebugLog.d(this, requestCode + " " + isSuccess);
    switch (requestCode) {
      case REQUEST_LOGIN:
        onLogin(isSuccess, response);
        break;
      case REQUEST_REGISTER:
        onRegister(isSuccess, response);
        break;
      case REQUEST_FORGOT_PASSWORD:
        onForgotPassword(isSuccess, response);
        break;
      case REQUEST_INTERESTS:
        onGetInterests(isSuccess, response);
        break;
      case REQUEST_INTERESTS_ADD:
        DebugLog.i(this, requestCode + " " + response);
        break;
      case REQUEST_BANNERS:
        onGetBanners(isSuccess, response);
        break;
      case REQUEST_AUCTIONS_UPCOMING:
      case REQUEST_AUCTIONS_PAST:
      case REQUEST_CALENDAR:
        onGetAuctions(requestCode, isSuccess, response);
        break;
      case REQUEST_LOTS:
        onGetLots(isSuccess, response);
        break;
      case REQUEST_FOLLOWS:
        onGetAuctions(requestCode, isSuccess, response);
        break;
      case REQUEST_FOLLOW_UPDATE:
        onFollowUpdate(isSuccess, response);
        break;
      case REQUEST_FOLLOW_CHECK:
        onFollowCheck(isSuccess, response);
        break;
      case REQUEST_SEARCH:
        onGetSearchResults(isSuccess, response);
        break;
      case REQUEST_CATEGORIES:
        onGetCategories(isSuccess, response);
        break;
      case REQUEST_CATEGORY_AUCTIONS:
        onGetAuctions(requestCode, isSuccess, response);
        break;
      case REQUEST_VIDEOS:
        onGetVideos(isSuccess, response);
        break;
      case REQUEST_PAGE:
        onGetPage(isSuccess, response);
        break;
    }
  }

  public interface OnLoginListener {
    void onLogin(boolean isSuccess, User user, String errorMsg);
  }

  public interface OnRegisterListener {
    void onRegister(boolean isSuccess, String message);
  }

  public interface OnGetInterestsListener {
    void onGetInterests(boolean isSuccess, boolean isInitiated);
  }

  public interface OnGetBannersListener {
    void onGetBanners(boolean isSuccess, List<Banner> banners);
  }

  public interface OnGetAuctionsListener {
    void onGetAuctions(boolean isSuccess, Auction.Type auctionType, List<Auction> auctions);
  }

  public interface OnGetLotsListener {
    void onGetLots(boolean isSuccess, List<Lot> lots);
  }

  public interface OnFollowUpdateListener {
    void onFollowUpdate(boolean isSuccess);
  }

  public interface OnFollowCheckListener {
    void onFollowCheck(boolean isSuccess, boolean isFollowed);
  }

  public interface OnGetSearchResultsListener {
    void onGetSearchResults(boolean isSuccess, int count, List<SearchResult> searchResults);
  }

  public interface OnGetCategoriesListener {
    void onGetCategories(boolean isSuccess, List<Category> categories);
  }

  public interface OnGetVideosListener {
    void onGetVideos(boolean isSuccess, List<Video> videos);
  }

  public interface OnGetPageListener {
    void onGetPage(boolean isSuccess, Page page);
  }
}
