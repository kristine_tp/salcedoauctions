package com.yondu.salcedoauctions.model;

import android.graphics.Bitmap;

import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.util.BitmapLoader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class Category implements BitmapLoader.OnLoadBitmapListener {

  public boolean isInterest;
  public int id, parentId;
  public String name, type, imageUri;
  public Bitmap image;
  private BitmapLoader mBitmapLoader;
  private List<BitmapLoader.OnLoadBitmapListener> mOnLoadBitmapListeners = new ArrayList<BitmapLoader.OnLoadBitmapListener>();

  public Category(JSONObject category) throws Exception {
    id = Integer.parseInt(category.getString("category_id"));
    parentId = Integer.parseInt(category.getString("category_parent_id"));
    name = category.getString("category_name");
    type = category.getString("category_type");
    imageUri = SalcedoAuctions.IMAGE_CATEGORIES + category.getString("category_image").replace(" ", "%20");
  }

  public void getCategoryBitmap(BitmapLoader.OnLoadBitmapListener listener) {
    if (listener != null && !mOnLoadBitmapListeners.contains(listener))
      mOnLoadBitmapListeners.add(listener);
    if (mBitmapLoader == null) mBitmapLoader = new BitmapLoader(imageUri, this);
  }

  public void interruptGetCategoryBitmap() {
    if (mBitmapLoader == null) mBitmapLoader.interrupt();
  }

  @Override
  public void onLoadBitmap(Bitmap image) {
    if (image != null) {
      this.image = image;
      for (BitmapLoader.OnLoadBitmapListener listener : mOnLoadBitmapListeners)
        listener.onLoadBitmap(image);
      mOnLoadBitmapListeners.clear();
    }
  }

}
