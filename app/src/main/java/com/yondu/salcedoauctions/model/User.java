package com.yondu.salcedoauctions.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kristine on 7/10/15.
 */
public class User {

  private static final String PREFS_NAME = "com.yondu.salcedoauctions.PREFS";
  private static final String PREFS_USER_ID = "user_id";
  private static final String PREFS_USER_EMAIL = "user_email";
  private static final String PREFS_USER_FIRSTNAME = "user_firstname";
  private static final String PREFS_USER_LASTNAME = "user_lastname";
  private static final String PREFS_USER_SALUTATION = "user_salutation";

  public int id = -1;
  public String email, firstname, lastname, salutation;
  private Context mContext;
  private static User mInstance;

  private List<OnUserStateChangeListener> mOnUserStateChangeListeners;

  public User(Context context) {
    mContext = context;
    getUserFromCache();
  }

  public User(int id, String email, String firstname, String lastname, String salutation) {
    this.id = id;
    this.email = email;
    this.firstname = firstname;
    this.lastname = lastname;
    this.salutation = salutation;
  }

  public static User getInstance(Context context) {
    if (mInstance == null) mInstance = new User(context);
    return mInstance;
  }

  public boolean isLoggedIn() {
    return email != null;
  }

  public void login(int id, String email, String firstname, String lastname, String salutation) {
    SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putInt(PREFS_USER_ID, id);
    editor.putString(PREFS_USER_EMAIL, email);
    editor.putString(PREFS_USER_FIRSTNAME, firstname);
    editor.putString(PREFS_USER_LASTNAME, lastname);
    editor.putString(PREFS_USER_SALUTATION, salutation);
    editor.commit();
    getUserFromCache();

    if (mOnUserStateChangeListeners != null) {
      for (OnUserStateChangeListener listener : mOnUserStateChangeListeners) {
        listener.onUserStateChange(isLoggedIn());
      }
    }
  }

  public void logout() {
    SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = prefs.edit();
    editor.clear();
    editor.commit();
    getUserFromCache();

    if (mOnUserStateChangeListeners != null) {
      for (OnUserStateChangeListener listener : mOnUserStateChangeListeners) {
        listener.onUserStateChange(isLoggedIn());
      }
    }
  }

  private void getUserFromCache() {
    SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    id = prefs.getInt(PREFS_USER_ID, -1);
    email = prefs.getString(PREFS_USER_EMAIL, null);
    firstname = prefs.getString(PREFS_USER_FIRSTNAME, null);
    lastname = prefs.getString(PREFS_USER_LASTNAME, null);
    salutation = prefs.getString(PREFS_USER_SALUTATION, null);
  }

  public void registerOnUserStateChangeListener(OnUserStateChangeListener listener) {
    if (mOnUserStateChangeListeners == null)
      mOnUserStateChangeListeners = new ArrayList<OnUserStateChangeListener>();
    if (!mOnUserStateChangeListeners.contains(listener)) mOnUserStateChangeListeners.add(listener);
  }

  public void unregisterOnUserStateChangeListener(OnUserStateChangeListener listener) {
    if (mOnUserStateChangeListeners != null) mOnUserStateChangeListeners.remove(listener);
  }

  public void unregisterOnUserStateChangeListeners() {
    if (mOnUserStateChangeListeners != null) mOnUserStateChangeListeners.clear();
  }

  public interface OnUserStateChangeListener {
    void onUserStateChange(boolean isLoggedIn);
  }
}
