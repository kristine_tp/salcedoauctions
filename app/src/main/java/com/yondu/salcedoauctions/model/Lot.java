package com.yondu.salcedoauctions.model;

import android.graphics.Bitmap;

import com.yondu.base.utils.DateHelper;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.util.BitmapLoader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class Lot implements BitmapLoader.OnLoadBitmapListener {

	public int id;
	public boolean isSold, isFollowed;
	public float bidStart, priceEstimate, priceSold;
	public String title, authorBrand, description, notes, tags, category, condition, imageUri;
	public Bitmap image;
	private BitmapLoader mBitmapLoader;
	private List<BitmapLoader.OnLoadBitmapListener> mOnLoadBitmapListeners = new ArrayList<BitmapLoader.OnLoadBitmapListener>();

	public Lot(JSONObject lot) throws Exception {
		id = Integer.parseInt(lot.getString("lot_id"));
		title = lot.getString("lot_title");
		authorBrand = lot.getString("lot_author_brand");
		description = lot.getString("lot_description");
		notes = lot.getString("lot_notes");
		tags = lot.getString("lot_tags");
		condition = lot.getString("lot_condition");
		bidStart = Float.parseFloat(lot.getString("lot_starting_bid"));
		priceEstimate = Float.parseFloat(lot.getString("lot_estimate_price"));
		priceSold = Float.parseFloat(lot.getString("lot_sold_price"));
		isSold = lot.getString("is_sold").equalsIgnoreCase("1");
		imageUri = SalcedoAuctions.API_HOST + lot.getString("image_file").replace(" ", "%20");
	}

	public void getLotBitmap(BitmapLoader.OnLoadBitmapListener listener) {
		if(listener != null && !mOnLoadBitmapListeners.contains(listener))
			mOnLoadBitmapListeners.add(listener);
		if(mBitmapLoader == null) mBitmapLoader = new BitmapLoader(imageUri, this);
	}

	@Override
	public void onLoadBitmap(Bitmap image) {
		if(image != null) {
			this.image = image;
			for(BitmapLoader.OnLoadBitmapListener listener : mOnLoadBitmapListeners)
				listener.onLoadBitmap(image);
			mOnLoadBitmapListeners.clear();
		}
	}
}
