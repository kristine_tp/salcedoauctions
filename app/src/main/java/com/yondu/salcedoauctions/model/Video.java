package com.yondu.salcedoauctions.model;

import android.graphics.Bitmap;

import com.yondu.base.utils.DateHelper;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.util.BitmapLoader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class Video implements BitmapLoader.OnLoadBitmapListener {

	public int id;
	public String title, videoUri, imageUri;
	public Calendar date;
	public Bitmap image;
	private BitmapLoader mBitmapLoader;
	private List<BitmapLoader.OnLoadBitmapListener> mOnLoadBitmapListeners = new ArrayList<BitmapLoader.OnLoadBitmapListener>();

	public Video(JSONObject video) throws Exception {
		id = Integer.parseInt(video.getString("video_id"));
		title = video.getString("video_description");
		videoUri = video.getString("video_link");
		date = DateHelper.getCalendarFromFormat(video.getString("created_at"), SalcedoAuctions.DATE_FORMAT);
		imageUri = SalcedoAuctions.API_HOST + video.getString("video_thumbnail").replace(" ", "%20");
	}

	public void getVideoBitmap(BitmapLoader.OnLoadBitmapListener listener) {
		if(listener != null && !mOnLoadBitmapListeners.contains(listener))
			mOnLoadBitmapListeners.add(listener);
		if(mBitmapLoader == null) mBitmapLoader = new BitmapLoader(imageUri, this);
	}

	@Override
	public void onLoadBitmap(Bitmap image) {
		if(image != null) {
			this.image = image;
			for(BitmapLoader.OnLoadBitmapListener listener : mOnLoadBitmapListeners)
				listener.onLoadBitmap(image);
			mOnLoadBitmapListeners.clear();
		}
	}
}
