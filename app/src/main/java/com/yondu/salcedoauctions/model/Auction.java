package com.yondu.salcedoauctions.model;

import android.graphics.Bitmap;

import com.yondu.base.utils.DateHelper;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.util.BitmapLoader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class Auction implements BitmapLoader.OnLoadBitmapListener {

  public enum Type {UPCOMING, PAST, CURRENT}

  public int id, phoneSlots;
  public boolean isFollowed;
  public String title, code, type, description, tags, categoryx = "", address = "", imageUri, videoLink;
  public Calendar date, previewDate;
  public Bitmap image;
  private Type auctionType;
  private BitmapLoader mBitmapLoader;
  private List<BitmapLoader.OnLoadBitmapListener> mOnLoadBitmapListeners = new ArrayList<BitmapLoader.OnLoadBitmapListener>();

  public Auction(JSONObject auction) throws Exception {
    id = Integer.parseInt(auction.getString("sale_id"));
    title = auction.getString("sale_title");
    code = auction.getString("sale_code");
    type = auction.getString("sale_type");
    description = auction.getString("sale_description").replace("\r\n", "<br/>").replace("\n", "<br/>");
    tags = auction.getString("sale_tags");
    imageUri = SalcedoAuctions.API_HOST + auction.getString("image_file").replace(" ", "%20");
    videoLink = auction.has("video_link") ? auction.getString("video_link") : "";
    date = DateHelper.getCalendarFromFormat(auction.getString("sale_auction_date"), SalcedoAuctions.DATE_FORMAT);
    previewDate = DateHelper.getCalendarFromFormat(auction.getString("sale_preview_date"), SalcedoAuctions.DATE_FORMAT);
    auctionType = getAuctionType();

    try {
      address = auction.getString("location_name");
      categoryx = auction.getString("category_name");
      phoneSlots = Integer.parseInt(auction.getString("sale_phone_slots"));
    } catch (Exception e) {
      DebugLog.eStackTrace(e);
    }
  }

  public Type getAuctionType() {
    Calendar today = Calendar.getInstance();
    if ((today.get(Calendar.YEAR) == date.get(Calendar.YEAR)) && (today.get(Calendar.MONTH) == date.get(Calendar.MONTH)) && (today.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH)))
      return Type.CURRENT;
    if (date.before(Calendar.getInstance())) return Type.PAST;
    return Type.UPCOMING;
  }

  public long getRemainingMillis() {
    auctionType = getAuctionType();
    if (auctionType == Type.UPCOMING)
      return date.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
    else return 0;
  }

  public void getAuctionBitmap(BitmapLoader.OnLoadBitmapListener listener) {
    if (listener != null && !mOnLoadBitmapListeners.contains(listener))
      mOnLoadBitmapListeners.add(listener);
    if (mBitmapLoader == null) mBitmapLoader = new BitmapLoader(imageUri, this);
  }

  public void interruptGetAuctionBitmap() {
    if (mBitmapLoader == null) mBitmapLoader.interrupt();
  }

  @Override
  public void onLoadBitmap(Bitmap image) {
    if (image != null) {
      this.image = image;
      for (BitmapLoader.OnLoadBitmapListener listener : mOnLoadBitmapListeners)
        listener.onLoadBitmap(image);
      mOnLoadBitmapListeners.clear();
    }
  }
}
