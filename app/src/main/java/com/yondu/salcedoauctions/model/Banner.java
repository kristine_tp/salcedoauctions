package com.yondu.salcedoauctions.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.yondu.base.utils.DateHelper;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.util.BitmapLoader;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class Banner implements BitmapLoader.OnLoadBitmapListener {

  public enum Type {LINK, AUCTION}

  public int id;
  public String title, event, imageUri, address, link;
  public Bitmap image;
  public Calendar date;
  public Type redirectType;
  private OnLoadBannerImageListener mOnLoadBannerImageListener;
  public Auction auction;

  public Banner(JSONObject banner) throws Exception {
    id = Integer.parseInt(banner.getString("banner_id"));
    title = banner.getString("banner_event");
    event = banner.getString("banner_name");
    link = banner.getString("banner_link");
    address = banner.getString("location_name");
    redirectType = Type.LINK;
    imageUri = SalcedoAuctions.API_HOST + banner.getString("image_file").replace(" ", "%20");
    date = DateHelper.getCalendarFromFormat(banner.getString("banner_date"), SalcedoAuctions.DATE_FORMAT);
  }

  public Banner(Auction auction) {
    this.auction = auction;
    id = auction.id;
    title = auction.title;
    event = "Current Auction";
    address = auction.address;
    redirectType = Type.AUCTION;
    imageUri = auction.imageUri;
    date = auction.date;
  }

  public void getBannerBitmap(final OnLoadBannerImageListener listener) {
    mOnLoadBannerImageListener = listener;
    new BitmapLoader(imageUri, this);
  }

  @Override
  public void onLoadBitmap(Bitmap image) {
    if (image != null) {
      this.image = image;
      if (mOnLoadBannerImageListener != null)
        mOnLoadBannerImageListener.onLoadBannerImage(true, this);
    }
  }

  public interface OnLoadBannerImageListener {
    void onLoadBannerImage(boolean isSuccess, Banner banner);
  }
}
