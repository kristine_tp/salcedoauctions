package com.yondu.salcedoauctions.model;

import org.json.JSONObject;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class Page {

	public int id;
	public String title, content;

	public Page(JSONObject page) throws Exception {
		id = Integer.parseInt(page.getString("page_id"));
		title = page.getString("page_title");
		content = page.getString("page_content");
	}

}
