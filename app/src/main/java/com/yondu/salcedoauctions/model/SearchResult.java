package com.yondu.salcedoauctions.model;

import org.json.JSONObject;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class SearchResult {

	public enum Type {AUCTION, LOT, SECTION}

	public Auction auction;
	public Lot lot;
	public String sectionName;
	public Type type;

	public SearchResult(Auction auction) {
		type = Type.AUCTION;
		this.auction = auction;
	}

	public SearchResult(Lot lot) {
		type = Type.LOT;
		this.lot = lot;
	}

	public SearchResult(String sectionName) {
		type = Type.SECTION;
		this.sectionName = sectionName;
	}

}
