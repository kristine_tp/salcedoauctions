package com.yondu.salcedoauctions.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.yondu.base.utils.DebugLog;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class BitmapLoader {

	private Thread loadThread;

	public BitmapLoader(final String imageUri, final OnLoadBitmapListener listener) {
		loadThread = new Thread(new Runnable() {
			public void run() {
				try {
					Bitmap image = BitmapFactory.decodeStream((InputStream) new URL(imageUri).getContent());
					if(listener != null) listener.onLoadBitmap(image);
				} catch(Exception e) {
					DebugLog.eStackTrace(e);
				}
			}
		});
		loadThread.start();
	}

	public void interrupt() {
		if(loadThread != null && loadThread.isAlive()) loadThread.interrupt();
	}

	public interface OnLoadBitmapListener {
			void onLoadBitmap(Bitmap image);
	}
}
