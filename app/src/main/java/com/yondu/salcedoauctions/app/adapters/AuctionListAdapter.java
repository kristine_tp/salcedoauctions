package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.utils.DateHelper;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class AuctionListAdapter extends ArrayAdapter<Auction> implements BitmapLoader.OnLoadBitmapListener {

	private Context mContext;
	private int mLayoutResource;
	private View mContainerView;

	public AuctionListAdapter(Context context, int layoutResource, List<Auction> auctions) {
		super(context, layoutResource, auctions);
		mContext = context;
		mLayoutResource = layoutResource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderItem viewHolder;
		if(convertView == null) {
			mContainerView = parent;
			convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
			viewHolder = new ViewHolderItem();
			viewHolder.monthTxt = (TextView) convertView.findViewById(R.id.txt_month);
			viewHolder.dateTxt = (TextView) convertView.findViewById(R.id.txt_date);
			viewHolder.timeTxt = (TextView) convertView.findViewById(R.id.txt_time);
			viewHolder.categoryTxt = (TextView) convertView.findViewById(R.id.txt_category);
			viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
			viewHolder.addressTxt = (TextView) convertView.findViewById(R.id.txt_address);
			viewHolder.thumbImg = (ImageView) convertView.findViewById(R.id.img_thumb);
			convertView.setTag(viewHolder);
		} else viewHolder = (ViewHolderItem) convertView.getTag();

		final Auction item = getItem(position);
		viewHolder.monthTxt.setText(item.date.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));
		viewHolder.dateTxt.setText(String.valueOf(item.date.get(Calendar.DAY_OF_MONTH)));
		viewHolder.timeTxt.setText((DateHelper.getFormattedDate(item.date, "EEE ha")));
		viewHolder.categoryTxt.setText(item.code);
		viewHolder.titleTxt.setText(item.title);
		viewHolder.addressTxt.setText(item.address);

		if(item.image != null) viewHolder.thumbImg.setImageBitmap(item.image);
		else {
			viewHolder.thumbImg.setImageResource(R.drawable.img_thumb_placeholder_light);
			item.getAuctionBitmap(this);
		}
		return convertView;
	}

	@Override
	public void onLoadBitmap(Bitmap image) {
		if(image != null) {
			mContainerView.post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}
	}

	static class ViewHolderItem {
		TextView monthTxt, dateTxt, timeTxt, categoryTxt, titleTxt, addressTxt;
		ImageView thumbImg;
	}
}