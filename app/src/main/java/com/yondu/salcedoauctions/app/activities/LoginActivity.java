package com.yondu.salcedoauctions.app.activities;

import android.content.Intent;
import android.os.Bundle;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleActionBarActivity;
import com.yondu.base.app.SimpleFragmentActivity;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.app.fragments.LoginFragment;
import com.yondu.salcedoauctions.model.User;

public class LoginActivity extends SimpleFragmentActivity {

  private boolean mIsUserLoggedIn;
  private User mUser;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    DebugLog.setTag("salcedo");
    mUser = User.getInstance(getApplicationContext());
    setInitialFragment(new LoginFragment(true));

    super.onCreate(savedInstanceState);
    mIsUserLoggedIn = mUser.isLoggedIn();
    if(mIsUserLoggedIn) startMainActivity();
  }

  private void startMainActivity() {
    startActivity(new Intent(getApplicationContext(), MainActivity.class));
    finish();
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    DebugLog.d(this, "onBackPressed");
    if (!mIsUserLoggedIn && mUser.isLoggedIn()) startMainActivity();
  }

}
