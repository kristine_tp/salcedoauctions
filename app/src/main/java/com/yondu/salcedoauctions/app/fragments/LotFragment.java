package com.yondu.salcedoauctions.app.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.base.utils.FormatUtils;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.activities.LotImageActivity;
import com.yondu.salcedoauctions.model.Lot;
import com.yondu.salcedoauctions.model.User;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.io.ByteArrayOutputStream;

/**
 * Created by Kristine Peromingan on 6/10/2015.
 */
public class LotFragment extends BaseActionBarFragment implements BitmapLoader.OnLoadBitmapListener, View.OnClickListener {

  private View mMainView;
  private SimpleAlertFragmentDialog mBidTermsDialog;
  private Lot mLot;
  private SalcedoAuctions mApi;
  private User mUser;

  public LotFragment() {}

  @SuppressLint("ValidFragment")
  public LotFragment(Lot lot) {
    mLot = lot;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if (mLot != null) {
      getBaseActionBar().setTitleText("Lot Details");
//      getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_share);
      getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
      getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);
//      updateActionBarView();

      if (mMainView == null) {
        mUser = User.getInstance(getContext());
        mApi = SalcedoAuctions.getInstance(getContext());
        mMainView = inflater.inflate(R.layout.fragment_lot, container, false);
        initViews();
        initContents();
      }
    } else this.backToPreviousFragment();
    return mMainView;
  }

//  private void updateActionBarView() {
//    getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2, mLot.isFollowed ? R.drawable.ic_star_gold : R.drawable.ic_star);
//  }

  private void initViews() {
    mMainView.findViewById(R.id.img_lot).setOnClickListener(this);
  }

  private void initContents() {
//    if (mUser.isLoggedIn())
//      mApi.followCheck(mUser.id, mLot.id, getWeakReferenceActivity(), this);

    ((TextView) mMainView.findViewById(R.id.txt_lot)).setText("Lot #" + mLot.id);
    ((TextView) mMainView.findViewById(R.id.txt_title)).setText(mLot.title);
    ((TextView) mMainView.findViewById(R.id.txt_artist)).setText("By " + mLot.authorBrand);
    ((TextView) mMainView.findViewById(R.id.txt_description)).setText(Html.fromHtml(mLot.description));

    if(mLot.isSold){
      ((TextView) mMainView.findViewById(R.id.lbl_price)).setText("Sold For");
      ((TextView) mMainView.findViewById(R.id.lbl_price)).setTextColor(getResources().getColor(R.color.red));
      ((TextView) mMainView.findViewById(R.id.txt_price)).setText("Php " + FormatUtils.formatCurrency(mLot.priceSold, false));
    }
    else {
      ((TextView) mMainView.findViewById(R.id.lbl_price)).setText("Estimated Value");
      ((TextView) mMainView.findViewById(R.id.lbl_price)).setTextColor(getResources().getColor(R.color.gray_light));
      ((TextView) mMainView.findViewById(R.id.txt_price)).setText("Php " + FormatUtils.formatCurrency(mLot.bidStart, false) + " - " + FormatUtils.formatCurrency(mLot.priceEstimate, false));
     }
    if (mLot.image != null)
      ((ImageView) mMainView.findViewById(R.id.img_lot)).setImageBitmap(mLot.image);
    else mLot.getLotBitmap(this);
  }

  private void startLotImageActivity() {
    if (mLot.image != null) {
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      mLot.image.compress(Bitmap.CompressFormat.JPEG, 70, stream);
      byte[] byteArray = stream.toByteArray();

      Intent intent = new Intent(getContext(), LotImageActivity.class);
      intent.putExtra(LotImageActivity.EXTRA_IMAGE, byteArray);
      startActivity(intent);
    }
  }

  private void showBidTermsDialog() {
    View bidTermsView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_lot_bid_terms, null);
    bidTermsView.findViewById(R.id.btn_close).setOnClickListener(this);
    mBidTermsDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
    mBidTermsDialog.setCustomView(bidTermsView);
    mBidTermsDialog.setCancelable(false);
    mBidTermsDialog.show(getFragmentManager(), null);
  }

  private void closeDialogs() {
    if (mBidTermsDialog != null) mBidTermsDialog.dismiss();
  }

//  @Override
//  public void onFollowUpdate(boolean isSuccess) {
//    if (isSuccess) {
//      mLot.isFollowed = !mLot.isFollowed;
//      updateActionBarView();
//    }
//  }
//
//  @Override
//  public void onFollowCheck(boolean isSuccess, boolean isFollowed) {
//    mLot.isFollowed = isFollowed;
//    updateActionBarView();
//  }

  @Override
  public void onLoadBitmap(Bitmap image) {
    if (image != null) {
      mMainView.post(new Runnable() {
        @Override
        public void run() {
          ((ImageView) mMainView.findViewById(R.id.img_lot)).setImageBitmap(mLot.image);
        }
      });
    }
  }

  @Override
  public void onRightActionButtonClicked(BaseActionBar.ActionButton which) {
    switch (which) {
      case BUTTON_RIGHT:
//				pushFragment(new SearchFragment());
        break;
//      case BUTTON_RIGHT_2:
//        if (mUser.isLoggedIn())
//          mApi.followUpdate(!mLot.isFollowed, mUser.id, mLot.id, getWeakReferenceActivity(), this);
//        break;
    }

  }

  @Override
  public void onClick(View view) {
    if (view.getId() == R.id.img_lot) startLotImageActivity();
    else if (view.getId() == R.id.btn_close) closeDialogs();
  }

}
