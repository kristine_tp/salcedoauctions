package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.utils.DateHelper;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.activities.MainActivity;
import com.yondu.salcedoauctions.app.adapters.CalendarAuctionListAdapter;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Lot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/10/2015.
 */
public class CalendarFragment extends CommonMenuFragment implements SalcedoAuctions.OnGetAuctionsListener,
	AdapterView.OnItemClickListener, View.OnClickListener {

	private View mMainView;
	private GridView mCalendarGridView;
	private CalendarAuctionListAdapter mCalendarAuctionListAdapter;
	private List<Auction> mCalendarAuctionList = new ArrayList<Auction>();
	private Calendar mCurrMonth = Calendar.getInstance();
	private SalcedoAuctions mApi;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getBaseActionBar().setTitleText("Calendar View");
		getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_search);
		getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

		if(mMainView == null) {
			mApi = SalcedoAuctions.getInstance(getContext());
			mMainView = inflater.inflate(R.layout.fragment_calendar, container, false);
			initViews();
			updateCalendar();
		}
		return mMainView;
	}

	private void initViews() {
		mCalendarGridView = (GridView) mMainView.findViewById(R.id.grid_calendar);
		mCalendarAuctionListAdapter = new CalendarAuctionListAdapter(getContext(), R.layout.view_calendar_item, mCalendarAuctionList);
		mCalendarGridView.setAdapter(mCalendarAuctionListAdapter);
		mCalendarGridView.setOnItemClickListener(this);

		mMainView.findViewById(R.id.btn_prev).setOnClickListener(this);
		mMainView.findViewById(R.id.btn_next).setOnClickListener(this);
	}

	private void updateCalendar() {
		((TextView) mMainView.findViewById(R.id.txt_month)).setText(DateHelper.getFormattedDate(mCurrMonth, "MMMM yyyy"));
		mApi.getCalendar(mCurrMonth.get(Calendar.MONTH) + 1, mCurrMonth.get(Calendar.YEAR), getWeakReferenceActivity(), this);

		for(Auction calendarAuction : mCalendarAuctionList) calendarAuction.interruptGetAuctionBitmap();
		mCalendarAuctionList.clear();
		mCalendarAuctionListAdapter.notifyDataSetChanged();
	}

	@Override
	public void onGetAuctions(boolean isSuccess, Auction.Type auctionType, List<Auction> auctions) {
		if(isSuccess && auctions != null) {
			mCalendarAuctionList.addAll(auctions);
			mCalendarAuctionListAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		pushFragment(new AuctionFragment(mCalendarAuctionList.get(position)));
	}

	@Override
	public void onClick(View view) {
		mCurrMonth.add(Calendar.MONTH, view.getId() == R.id.btn_next ? 1 : -1);
		updateCalendar();
	}

	@Override
	public void onRightActionButtonClicked(BaseActionBar.ActionButton which) {
		DebugLog.d(this, which.name());
		switch(which) {
			case BUTTON_RIGHT:
				pushFragment(new SearchFragment());
				break;
		}
	}
}
