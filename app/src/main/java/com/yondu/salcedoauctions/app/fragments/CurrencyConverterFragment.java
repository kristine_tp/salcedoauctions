package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.salcedoauctions.R;

/**
 * Created by Kristine Peromingan on 6/30/2015.
 */
public class CurrencyConverterFragment extends CommonMenuFragment implements View.OnClickListener {

	private View mMainView;
	private SimpleAlertFragmentDialog mAddCurrencyDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getBaseActionBar().setTitleText("Currency Converter");
		getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
		getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

		if(mMainView == null) {
			mMainView = inflater.inflate(R.layout.fragment_currency_converter, container, false);
			initViews();
		}
		return mMainView;
	}

	private void initViews() {
		mMainView.findViewById(R.id.btn_add).setOnClickListener(this);
	}

	private void showAddCurrencyDialog() {
		View addCurrencyView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_currency_converter_add, null);
		addCurrencyView.findViewById(R.id.btn_close).setOnClickListener(this);
		mAddCurrencyDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
		mAddCurrencyDialog.setCustomView(addCurrencyView);
		mAddCurrencyDialog.setCancelable(false);
		mAddCurrencyDialog.show(getFragmentManager(), null);
	}

	private void closeDialogs() {
		if(mAddCurrencyDialog != null) mAddCurrencyDialog.dismiss();
	}


	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_add) showAddCurrencyDialog();
		else if(view.getId() == R.id.btn_close) closeDialogs();
	}

}
