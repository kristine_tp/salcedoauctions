package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yondu.base.utils.DateHelper;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Banner;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Kristine Peromingan on 7/2/2015.
 */
public class BannerPagerAdapter extends PagerAdapter {

	private int mLayoutResId;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<Banner> mBanners;
	private OnPageClickListener mOnItemClickListener;

	public BannerPagerAdapter(Context context, int layoutResId, List<Banner> banners, OnPageClickListener listener) {
		mContext = context;
		mBanners = banners;
		mLayoutResId = layoutResId;
		mOnItemClickListener = listener;
		mLayoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return mBanners.size();
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((View) object);
	}

	@Override
	public Object instantiateItem(final ViewGroup container, final int position) {
		View itemView = mLayoutInflater.inflate(mLayoutResId, container, false);
		container.addView(itemView);

		Banner banner = mBanners.get(position);
		((TextView) itemView.findViewById(R.id.txt_event)).setText(banner.event);
		((TextView) itemView.findViewById(R.id.txt_title)).setText(banner.title);
		((TextView) itemView.findViewById(R.id.txt_month)).setText(banner.date.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US));
		((TextView) itemView.findViewById(R.id.txt_day)).setText(String.valueOf(banner.date.get(Calendar.DAY_OF_MONTH)));
		((TextView) itemView.findViewById(R.id.txt_time)).setText(DateHelper.getFormattedDate(banner.date, "EEE ha"));
		((TextView) itemView.findViewById(R.id.txt_address)).setText(banner.address);
		if(banner.image != null)
			((ImageView) itemView.findViewById(R.id.img_banner)).setImageBitmap(banner.image);

		itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(mOnItemClickListener != null)
					mOnItemClickListener.onPageClick(container, view, position);
			}
		});

		return itemView;
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	public static interface OnPageClickListener {
		void onPageClick(ViewGroup parent, View view, int position);
	}
}
