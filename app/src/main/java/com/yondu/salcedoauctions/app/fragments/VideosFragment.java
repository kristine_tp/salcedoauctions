package com.yondu.salcedoauctions.app.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.adapters.VideoListAdapter;
import com.yondu.salcedoauctions.model.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/29/2015.
 */
public class VideosFragment extends CommonMenuFragment implements SalcedoAuctions.OnGetVideosListener, VideoListAdapter.OnClickVideoItemListener {

    private View mMainView;
    private ListView mVideoListView;
    private VideoListAdapter mVideoListAdapter;
    private List<Video> mVideoList = new ArrayList<Video>();
    private SalcedoAuctions mApi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getBaseActionBar().setTitleText("Videos");
        getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
        getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

        if (mMainView == null) {
            mApi = SalcedoAuctions.getInstance(getContext());
            mMainView = inflater.inflate(R.layout.fragment_videos, container, false);
            initVideos();
        }
        return mMainView;
    }

    private void initVideos() {
        mApi.getVideos(getWeakReferenceActivity(), this);
        mVideoListView = (ListView) mMainView.findViewById(R.id.list_videos);
        mVideoListAdapter = new VideoListAdapter(getContext(), R.layout.view_videos_item, mVideoList, this);
        mVideoListView.setAdapter(mVideoListAdapter);
    }

    @Override
    public void onGetVideos(boolean isSuccess, List<Video> videos) {
        if (isSuccess && videos != null) {
//            ((TextView) mMainView.findViewById(R.id.txt_count)).setText(videos.size() + " Videos");
            mVideoList.clear();
            mVideoList.addAll(videos);
            mVideoListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClickVideoItem(Video video) {
        if(video.videoUri != null) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(video.videoUri));
            startActivity(intent);
        }
    }
}
