package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.utils.DebugLog;
import com.yondu.base.utils.FormatUtils;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Category;
import com.yondu.salcedoauctions.model.Lot;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class CategoryListAdapter extends ArrayAdapter<Category> implements BitmapLoader.OnLoadBitmapListener {

	private Context mContext;
	private int mLayoutResource;
	private View mContainerView;

	public CategoryListAdapter(Context context, int layoutResource, List<Category> categories) {
		super(context, layoutResource, categories);
		mContext = context;
		mLayoutResource = layoutResource;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolderItem viewHolder;
		if(convertView == null) {
			mContainerView = parent;
			convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
			viewHolder = new ViewHolderItem();
			viewHolder.categoryTxt = (TextView) convertView.findViewById(R.id.txt_category);
			viewHolder.categoryImg = (ImageView) convertView.findViewById(R.id.img_category);
			convertView.setTag(viewHolder);
		} else viewHolder = (ViewHolderItem) convertView.getTag();

		final Category item = getItem(position);
		viewHolder.categoryTxt.setText(item.name);

		if(item.image != null) viewHolder.categoryImg.setImageBitmap(item.image);
		else {
			viewHolder.categoryImg.setImageResource(android.R.color.transparent);
			item.getCategoryBitmap(this);
		}
		return convertView;
	}

	@Override
	public void onLoadBitmap(Bitmap image) {
		if(image != null) {
			mContainerView.post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}
	}

	static class ViewHolderItem {
		TextView categoryTxt;
		ImageView categoryImg;
	}
}