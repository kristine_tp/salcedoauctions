package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.adapters.FollowsListAdapter;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/29/2015.
 */
public class FollowedAuctionsFragment extends CommonMenuFragment implements SalcedoAuctions.OnGetAuctionsListener, AdapterView.OnItemClickListener,
    View.OnClickListener {

  private ListView mFollowsListView;
  private FollowsListAdapter mFollowsListAdapter;
  private List<Auction> mFollowsList = new ArrayList<Auction>();
  private SalcedoAuctions mApi;
  private User mUser;
  private Handler mListCountdownHandler = new Handler();
  private SimpleAlertFragmentDialog mLoginRequiredDialog;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getBaseActionBar().setTitleText("Auctions I Follow");
    getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
    getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

    if(mFollowsListView == null) {
      mUser = User.getInstance(getContext());
      mApi = SalcedoAuctions.getInstance(getContext());
      mFollowsListView = (ListView) inflater.inflate(R.layout.fragment_common_listview, container, false);
      initFollows();
    }
    return mFollowsListView;
  }

  private void getFollowedAuctions() {
    if(mUser.isLoggedIn()) {
      if (mFollowsList.isEmpty()) mApi.getFollows(mUser.id, getWeakReferenceActivity(), this);
    }
    else showLoginFirstDialog();
  }

  @Override
  public void onResume() {
    super.onResume();
    setListCountdownUpdater();
    getFollowedAuctions();
  }

  @Override
  public void onPause() {
    super.onPause();
    mListCountdownHandler.removeCallbacks(mListCountdownUpdater);
  }

  private void initFollows() {
    mFollowsListAdapter = new FollowsListAdapter(getContext(), R.layout.view_auctions_followed_item, mFollowsList);
    mFollowsListView.setAdapter(mFollowsListAdapter);
    mFollowsListView.setOnItemClickListener(this);
  }

  private void setListCountdownUpdater() {
    if(!mFollowsList.isEmpty()) {
      mListCountdownHandler.removeCallbacks(mListCountdownUpdater);
      mListCountdownHandler.postDelayed(mListCountdownUpdater, 1000);
    }
  }

  private void showLoginFirstDialog() {
    View loginRequiredView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_login_required_dialog, null);
    loginRequiredView.findViewById(R.id.btn_close).setOnClickListener(this);
    loginRequiredView.findViewById(R.id.btn_login).setOnClickListener(this);
    mLoginRequiredDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
    mLoginRequiredDialog.setCustomView(loginRequiredView);
    mLoginRequiredDialog.setCancelable(false);
    mLoginRequiredDialog.show(getFragmentManager(), null);
  }

  private void closeDialog() {
    if(mLoginRequiredDialog != null) mLoginRequiredDialog.dismiss();
  }

  @Override
  public void onGetAuctions(boolean isSuccess, Auction.Type auctionType, List<Auction> auctions) {
    if(isSuccess && auctions != null) {
      mFollowsList.clear();
      mFollowsList.addAll(auctions);
      mFollowsListAdapter.notifyDataSetChanged();
      setListCountdownUpdater();
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    pushFragment(new AuctionFragment(mFollowsList.get(position)));
  }

  @Override
  public void onClick(View view) {
    if(view.getId() == R.id.btn_close) {
      closeDialog();
      getActivity().onBackPressed();
    } else if(view.getId() == R.id.btn_login) {
      closeDialog();
      pushFragment(new LoginFragment());
    }
  }

  private Runnable mListCountdownUpdater = new Runnable() {
    @Override
    public void run() {
      mFollowsListAdapter.notifyDataSetChanged();
      for(Auction followedAuction : mFollowsList) {
        if(followedAuction.getAuctionType() == Auction.Type.UPCOMING) {
          mFollowsListAdapter.notifyDataSetChanged();
          mListCountdownHandler.postDelayed(mListCountdownUpdater, 1000);
          return;
        }
      }
    }
  };

}
