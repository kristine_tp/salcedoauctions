package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.utils.FormatUtils;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Lot;
import com.yondu.salcedoauctions.model.SearchResult;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class SearchResultListAdapter extends ArrayAdapter<SearchResult> {

	private Context mContext;
	private int mLayoutResource;
	private View mContainerView;

	public SearchResultListAdapter(Context context, int layoutResource, List<SearchResult> searchResults) {
		super(context, layoutResource, searchResults);
		mContext = context;
		mLayoutResource = layoutResource;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolderItem viewHolder;
		if(convertView == null) {
			mContainerView = parent;
			convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
			viewHolder = new ViewHolderItem();
			viewHolder.detailsView = convertView.findViewById(R.id.grp_details);
			viewHolder.sectionTxt = (TextView) convertView.findViewById(R.id.txt_section);
			viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
			viewHolder.labelTxt = (TextView) convertView.findViewById(R.id.txt_label);
			viewHolder.statusTxt = (TextView) convertView.findViewById(R.id.txt_status);
			convertView.setTag(viewHolder);
		} else viewHolder = (ViewHolderItem) convertView.getTag();

		SearchResult item = getItem(position);
		if(item.type == SearchResult.Type.SECTION) {
			convertView.setClickable(true);
			viewHolder.detailsView.setVisibility(View.GONE);
			viewHolder.sectionTxt.setVisibility(View.VISIBLE);
			viewHolder.sectionTxt.setText(item.sectionName);
		} else {
			convertView.setClickable(false);
			viewHolder.sectionTxt.setVisibility(View.GONE);
			viewHolder.detailsView.setVisibility(View.VISIBLE);
			if(item.type == SearchResult.Type.AUCTION) {
				viewHolder.titleTxt.setText(item.auction.title);
//				viewHolder.labelTxt.setText(item.auction.category.isEmpty() ? item.auction.tags : item.auction.category);
				viewHolder.labelTxt.setText(item.auction.code);
				viewHolder.labelTxt.setVisibility(viewHolder.labelTxt.getText().length() == 0 ? View.GONE : View.VISIBLE);
				viewHolder.statusTxt.setText(item.auction.getAuctionType() == Auction.Type.UPCOMING ? "Upcoming" : "");
			} else if(item.type == SearchResult.Type.LOT) {
				viewHolder.titleTxt.setText(item.lot.title);
				viewHolder.labelTxt.setText(item.lot.authorBrand);
				viewHolder.labelTxt.setVisibility(View.VISIBLE);
				viewHolder.statusTxt.setText(item.lot.isSold ? "Sold" : "");
			}
		}
		return convertView;
	}

	static class ViewHolderItem {
		TextView sectionTxt, titleTxt, labelTxt, statusTxt;
		View detailsView;
	}
}