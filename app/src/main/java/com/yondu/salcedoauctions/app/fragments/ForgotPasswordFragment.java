package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.base.utils.Connectivity;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;

/**
 * Created by Kristine Peromingan on 6/10/2015.
 */
public class ForgotPasswordFragment extends BaseActionBarFragment implements SimpleAlertFragmentDialog.OnDialogDismissListener, View.OnClickListener {

  private View mMainView;
  private EditText mEmailInput;
  private Animation mInputErrorAnim;
  private SimpleAlertFragmentDialog mEmailedDialog;
  private SalcedoAuctions mApi;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getBaseActionBar().setTitleText("Forgot Password");
    getBaseActionBar().setHomeAction(BaseActionBar.ActionHome.BACK, R.drawable.ic_back);

    if (mMainView == null) {
      mApi = SalcedoAuctions.getInstance(getContext());
      mMainView = inflater.inflate(R.layout.fragment_login_forgot_password, container, false);
      initViews();
    }
    return mMainView;
  }

  private void initViews() {
    mEmailInput = (EditText) mMainView.findViewById(R.id.input_email);
    mInputErrorAnim = AnimationUtils.loadAnimation(getContext(), R.anim.input_error_shake);
    mMainView.findViewById(R.id.btn_submit).setOnClickListener(this);
  }

  private void showSentEmail() {
    LayoutInflater inflater = LayoutInflater.from(getContext());
    View emailedView = inflater.inflate(R.layout.dialog_login_email_password, null);
    emailedView.findViewById(R.id.btn_close).setOnClickListener(this);

    mEmailedDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL);
    mEmailedDialog.setCustomView(emailedView);
    mEmailedDialog.setDismissListener(this);
    mEmailedDialog.setCancelable(false);
    mEmailedDialog.show(getFragmentManager(), null);
  }

  private void onClickSubmit() {
    if (isInputValid() && Connectivity.isConnected(getWeakReferenceActivity(), true)) {
      showLoading(true);
      mApi.forgotPassword(mEmailInput.getText().toString(), getWeakReferenceActivity(), null);
    }
  }

  private boolean isInputValid() {
    if (!Patterns.EMAIL_ADDRESS.matcher(mEmailInput.getText()).matches()) {
      mMainView.findViewById(R.id.grp_email).startAnimation(mInputErrorAnim);
      return false;
    }
    return true;
  }

  @Override
  public void onClick(View view) {
    if (view.getId() == R.id.btn_submit) onClickSubmit();
    else if (view.getId() == R.id.btn_close) mEmailedDialog.dismiss();
  }

  @Override
  public void onDialogDismiss(SimpleAlertFragmentDialog dialog) {
    backToPreviousFragment();
  }
}
