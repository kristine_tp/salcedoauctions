package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.activities.MainActivity;
import com.yondu.salcedoauctions.app.adapters.SearchResultListAdapter;
import com.yondu.salcedoauctions.model.SearchResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/1/2015.
 */
public class SearchFragment extends BaseActionBarFragment implements SalcedoAuctions.OnGetSearchResultsListener, TextWatcher,
	AdapterView.OnItemClickListener, View.OnKeyListener, View.OnClickListener {

	private View mMainView, mOptionsView, mResultsView, mResultsLoad;
	private TextView mResultsTxt;
	private EditText mSearchInput;
	private ListView mSearchResultsListView;
	private SearchResultListAdapter mSearchResultListAdapter;
	private List<SearchResult> mSearchResultsList = new ArrayList<SearchResult>();
	private SalcedoAuctions mApi;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getBaseActionBar().setTitleText("Search");
//		getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_refresh);
		getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
		getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

		if(mMainView == null) {
			mApi = SalcedoAuctions.getInstance(getContext());
			mMainView = inflater.inflate(R.layout.fragment_search, container, false);
			initViews();
		}
		return mMainView;
	}

	private void initViews() {
		mSearchInput = (EditText) mMainView.findViewById(R.id.input_search);
		mResultsTxt = (TextView) mMainView.findViewById(R.id.txt_results);
		mOptionsView = mMainView.findViewById(R.id.grp_default);
		mResultsView = mMainView.findViewById(R.id.grp_results);
		mResultsLoad = mMainView.findViewById(R.id.load_results);

		mSearchResultsListView = (ListView) mMainView.findViewById(R.id.list_results);
		mSearchResultListAdapter = new SearchResultListAdapter(getContext(), R.layout.view_search_item, mSearchResultsList);
		mSearchResultsListView.setAdapter(mSearchResultListAdapter);
		mSearchResultsListView.setOnItemClickListener(this);

		mSearchInput.setOnKeyListener(this);
		mSearchInput.addTextChangedListener(this);
		mMainView.findViewById(R.id.btn_clear).setOnClickListener(this);
		mMainView.findViewById(R.id.btn_auctions).setOnClickListener(this);
		mMainView.findViewById(R.id.btn_past).setOnClickListener(this);
		mMainView.findViewById(R.id.btn_contact).setOnClickListener(this);
	}

	private void startSearch() {
		if(mSearchInput.getText().length() > 0) {
			mApi.search(mSearchInput.getText().toString(), getWeakReferenceActivity(), this);
			if(mResultsView.getVisibility() != View.VISIBLE) {
				mSearchResultsList.clear();
				mSearchResultListAdapter.notifyDataSetChanged();
			}

			mResultsTxt.setText("Searching");
//			mOptionsView.setVisibility(View.GONE);
			mResultsView.setVisibility(View.VISIBLE);
			mResultsLoad.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onGetSearchResults(boolean isSuccess, int count, List<SearchResult> searchResults) {
		mResultsLoad.setVisibility(View.GONE);
		if(isSuccess) {
			mResultsTxt.setText(count + " Results Found");
			if(!searchResults.isEmpty()) {
				mSearchResultsList.clear();
				mSearchResultsList.addAll(searchResults);
				mSearchResultListAdapter.notifyDataSetChanged();
			}
		} else mResultsTxt.setText("Search Error");
	}

	@Override
	public void onTextChanged(CharSequence text, int start, int before, int count) {
		if(text.length() == 0) {
//			mOptionsView.setVisibility(View.VISIBLE);
			mResultsView.setVisibility(View.GONE);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence text, int start, int count, int after) {}

	@Override
	public void afterTextChanged(Editable text) {}

	@Override
	public boolean onKey(View view, int keyCode, KeyEvent event) {
		if(view.getId() == R.id.input_search) {
			if(keyCode == EditorInfo.IME_ACTION_SEARCH || keyCode == EditorInfo.IME_ACTION_DONE || (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
				startSearch();
				return true;
			}
		}
		return false;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		DebugLog.d(this, "" + position);
		SearchResult result = mSearchResultsList.get(position);
		if(result.type == SearchResult.Type.AUCTION) pushFragment(new AuctionFragment(result.auction));
		else if(result.type == SearchResult.Type.LOT) pushFragment(new LotFragment(result.lot));
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_clear) mSearchInput.setText("");
		else if(view.getId() == R.id.btn_auctions)
			((MainActivity) getActivity()).switchMenu(MainActivity.Menu.AUCTIONS);
		else if(view.getId() == R.id.btn_past)
			((MainActivity) getActivity()).switchMenu(MainActivity.Menu.PAST_AUCTIONS);
		else if(view.getId() == R.id.btn_contact)
			((MainActivity) getActivity()).switchMenu(MainActivity.Menu.CONTACT);
	}

}
