package com.yondu.salcedoauctions.app.activities;

import android.os.Bundle;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleActionBarActivity;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.app.fragments.ForgotPasswordFragment;
import com.yondu.salcedoauctions.app.fragments.RegisterFragment;

public class ForgotPasswordActivity extends SimpleActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    setActionBarHomeButton(R.drawable.ic_back);
    setActionHome(BaseActionBar.ActionHome.BACK);
    setInitialFragment(new ForgotPasswordFragment());
    super.onCreate(savedInstanceState);
  }
}
