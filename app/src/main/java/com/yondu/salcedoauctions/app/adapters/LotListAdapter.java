package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.utils.FormatUtils;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Lot;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class LotListAdapter extends ArrayAdapter<Lot> implements BitmapLoader.OnLoadBitmapListener {

  private Context mContext;
  private int mLayoutResource;
  private View mContainerView;

  public LotListAdapter(Context context, int layoutResource, List<Lot> lots) {
    super(context, layoutResource, lots);
    mContext = context;
    mLayoutResource = layoutResource;
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    final ViewHolderItem viewHolder;
    if(convertView == null) {
      mContainerView = parent;
      convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
      viewHolder = new ViewHolderItem();
      viewHolder.numTxt = (TextView) convertView.findViewById(R.id.txt_num);
      viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
      viewHolder.statusTxt = (TextView) convertView.findViewById(R.id.txt_status);
      viewHolder.priceTxt = (TextView) convertView.findViewById(R.id.txt_price);
      viewHolder.priceLabelTxt = (TextView) convertView.findViewById(R.id.txt_price_label);
      viewHolder.starImg = (ImageView) convertView.findViewById(R.id.img_star);
      viewHolder.thumbImg = (ImageView) convertView.findViewById(R.id.img_thumb);
      convertView.setTag(viewHolder);
    } else viewHolder = (ViewHolderItem) convertView.getTag();

    final Lot item = getItem(position);
    viewHolder.numTxt.setText("Lot #" + item.id);
    viewHolder.titleTxt.setText(item.title);
    viewHolder.statusTxt.setText(item.isSold ? "Sold" : "");
    viewHolder.priceLabelTxt.setText(item.isSold ? "Sold For" : "Estimate");
    viewHolder.priceTxt.setText("Php " + FormatUtils.formatCurrency(item.isSold ? item.priceSold : item.priceEstimate, false));
    //		viewHolder.starImg.setImageResource(item.isFollowed ? R.drawable.ic_star_gold : R.drawable.ic_star);

    if(item.image != null) viewHolder.thumbImg.setImageBitmap(item.image);
    else {
      viewHolder.thumbImg.setImageResource(R.drawable.img_thumb_placeholder_dark);
      item.getLotBitmap(this);
    }

    return convertView;
  }

  @Override
  public void onLoadBitmap(Bitmap image) {
    if(image != null) {
      mContainerView.post(new Runnable() {
        @Override
        public void run() {
          notifyDataSetChanged();
        }
      });
    }
  }

  static class ViewHolderItem {
    TextView numTxt, titleTxt, statusTxt, priceTxt, priceLabelTxt;
    ImageView starImg, thumbImg;
  }
}