package com.yondu.salcedoauctions.app.activities;

import android.content.Intent;
import android.os.Bundle;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleActionBarActivity;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.app.fragments.LoginFragment;
import com.yondu.salcedoauctions.app.fragments.RegisterFragment;
import com.yondu.salcedoauctions.model.User;

public class RegisterActivity extends SimpleActionBarActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    setActionBarHomeButton(R.drawable.ic_back);
    setActionHome(BaseActionBar.ActionHome.BACK);
    setInitialFragment(new RegisterFragment());
    super.onCreate(savedInstanceState);
  }
}
