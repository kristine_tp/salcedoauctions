package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.views.FontCheckedTextView;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Category;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class InterestListAdapter extends ArrayAdapter<Category> {

  private Context mContext;
  private int mLayoutResource;

  public InterestListAdapter(Context context, int layoutResource, List<Category> categories) {
    super(context, layoutResource, categories);
    mContext = context;
    mLayoutResource = layoutResource;
  }

  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    if (convertView == null)
      convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);

    final Category item = getItem(position);
    ((FontCheckedTextView) convertView).setText(item.name);
    ((FontCheckedTextView) convertView).setChecked(item.isInterest);
    return convertView;
  }
}