package com.yondu.salcedoauctions.app.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.BaseFragmentActivity;
import com.yondu.base.utils.DebugLog;
import com.yondu.base.views.TabViews;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.activities.MainActivity;
import com.yondu.salcedoauctions.app.adapters.AuctionListAdapter;
import com.yondu.salcedoauctions.app.adapters.BannerPagerAdapter;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Banner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/6/2015.
 */
public class HomeFragment extends BaseActionBarFragment implements SalcedoAuctions.OnGetBannersListener, SalcedoAuctions.OnGetAuctionsListener,
    BannerPagerAdapter.OnPageClickListener, Banner.OnLoadBannerImageListener, ViewPager.OnPageChangeListener, SlidingUpPanelLayout.PanelSlideListener,
    TabViews.OnTabSwitchListener, AdapterView.OnItemClickListener, ViewTreeObserver.OnGlobalLayoutListener {

  private boolean mIsTabsInitiated, mIsBannersLoaded, mIsUpcomingAuctionsLoaded, mIsPastAuctionsLoaded;
  private Auction.Type mCurrAuctionTypeTab = Auction.Type.UPCOMING;
  private View mMainView, mBannerFullView, mBannerMinView;
  private ViewGroup mBannerIndicatorsFullView, mBannerIndicatorsMinView;
  private TabViews mAuctionTabs;
  private ViewPager mBannerFullPager, mBannerMinPager;
  private BannerPagerAdapter mBannerFullAdapter, mBannerMinAdapter;
  private List<Banner> mBannerList = new ArrayList<Banner>();
  private ListView mAuctionListView;
  private AuctionListAdapter mAuctionListAdapter;
  private List<Auction> mAuctionList = new ArrayList<Auction>(), mUpcomingAuctionList, mPastAuctionList;
  private SlidingUpPanelLayout mSliderAuctions;
  private SalcedoAuctions mApi;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getBaseActionBar().setTitleImg(R.drawable.ic_actionbar_logo);
    getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_search);
    getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2, R.drawable.ic_calendar);

    if (mMainView == null) {
      mApi = SalcedoAuctions.getInstance(getContext());
      mMainView = inflater.inflate(R.layout.fragment_home, container, false);
      initViews();
    }
    return mMainView;
  }

  private void initContents() {
    if (!mIsBannersLoaded) mApi.getBanners(getWeakReferenceActivity(), this);
    if (!mIsUpcomingAuctionsLoaded)
      mApi.getAuctions(Auction.Type.UPCOMING, getWeakReferenceActivity(), this);
    else if (!mIsPastAuctionsLoaded)
      mApi.getAuctions(Auction.Type.PAST, getWeakReferenceActivity(), this);
  }

  private void initViews() {
    initBanners();
    initAuctions();
    mMainView.getViewTreeObserver().addOnGlobalLayoutListener(this);
    mAuctionTabs = (TabViews) mMainView.findViewById(R.id.tab_auctions);
    mSliderAuctions = (SlidingUpPanelLayout) mMainView.findViewById(R.id.slider_auctions);
    mBannerIndicatorsFullView = (ViewGroup) mMainView.findViewById(R.id.grp_indicators_full);
    mBannerIndicatorsMinView = (ViewGroup) mMainView.findViewById(R.id.grp_indicators_min);

    mAuctionTabs.setOnTabSwitchListener(this);
    mSliderAuctions.setPanelSlideListener(this);
  }

  private void initBanners() {
    mBannerFullView = mMainView.findViewById(R.id.grp_banner_full);
    mBannerFullPager = (ViewPager) mMainView.findViewById(R.id.pager_banners_full);
    mBannerFullAdapter = new BannerPagerAdapter(getContext(), R.layout.view_home_banner_full_item, mBannerList, this);
    mBannerFullPager.setAdapter(mBannerFullAdapter);
    mBannerFullPager.setOnPageChangeListener(this);

    mBannerMinView = mMainView.findViewById(R.id.grp_banner_min);
    mBannerMinPager = (ViewPager) mMainView.findViewById(R.id.pager_banners_min);
    mBannerMinAdapter = new BannerPagerAdapter(getContext(), R.layout.view_home_banner_min_item, mBannerList, this);
    mBannerMinPager.setAdapter(mBannerMinAdapter);
    mBannerMinPager.setOnPageChangeListener(this);
  }

  private void initAuctions() {
    mAuctionListView = (ListView) mMainView.findViewById(R.id.list_auctions);
    mAuctionListAdapter = new AuctionListAdapter(getContext(), R.layout.view_home_auctions_item, mAuctionList);
    mAuctionListView.setAdapter(mAuctionListAdapter);
    mAuctionListView.setOnItemClickListener(this);
  }

  private void updateAuctionList() {
    mAuctionList.clear();
    List<Auction> auctions = mCurrAuctionTypeTab == Auction.Type.UPCOMING ? mUpcomingAuctionList : mPastAuctionList;
    if (auctions != null) mAuctionList.addAll(auctions);
    else mAuctionList.clear();
    mAuctionListAdapter.notifyDataSetChanged();
  }

  private void setCurrentAuctionsAsBanners() {
    if (mUpcomingAuctionList != null) {
      for (int i = 0; i < mUpcomingAuctionList.size(); i++) {
        Auction auction = mUpcomingAuctionList.get(i);
        if (auction.getAuctionType() == Auction.Type.CURRENT) addBanner(i, new Banner(auction));
      }
      mBannerFullAdapter.notifyDataSetChanged();
      mBannerMinAdapter.notifyDataSetChanged();
    }
  }

  private void addBanner(int position, Banner banner) {
    if (position != -1) mBannerList.add(position, banner);
    else mBannerList.add(banner);
    banner.getBannerBitmap(this);
    mBannerIndicatorsFullView.addView(LayoutInflater.from(getContext()).inflate(R.layout.view_home_auction_indicator, null));
    mBannerIndicatorsMinView.addView(LayoutInflater.from(getContext()).inflate(R.layout.view_home_auction_indicator, null));
  }

  public void setInitState() {
    try {
      mIsTabsInitiated = false;
      mAuctionTabs.switchTab(0);
      mSliderAuctions.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    } catch (NullPointerException e) {
      DebugLog.eStackTrace(e);
    }
  }

  public void setTab(Auction.Type auction) {
    mAuctionTabs.switchTab(auction == Auction.Type.PAST ? 1 : 0);
  }

  @Override
  public void onGetBanners(boolean isSuccess, List<Banner> banners) {
    if (isSuccess && banners != null && !banners.isEmpty()) {
      mIsBannersLoaded = true;
      for (int i = 0; i < banners.size(); i++) addBanner(-1, banners.get(i));
      onPageSelected(0);
      mBannerFullAdapter.notifyDataSetChanged();
      mBannerMinAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onLoadBannerImage(boolean isSuccess, Banner banner) {
    if (isSuccess) {
      mMainView.post(new Runnable() {
        public void run() {
          mBannerFullAdapter.notifyDataSetChanged();
          mBannerMinAdapter.notifyDataSetChanged();
        }
      });
    }
  }

  @Override
  public void onGetAuctions(boolean isSuccess, Auction.Type auctionType, List<Auction> auctions) {
    if (isSuccess && auctions != null) {
      if (auctionType == Auction.Type.UPCOMING) {
        mIsUpcomingAuctionsLoaded = true;
        mUpcomingAuctionList = auctions;
        setCurrentAuctionsAsBanners();
        mApi.getAuctions(Auction.Type.PAST, getWeakReferenceActivity(), this);
      } else if (auctionType == Auction.Type.PAST) {
        mIsPastAuctionsLoaded = true;
        mPastAuctionList = auctions;
      }
      updateAuctionList();
      for (Auction auction : auctions) auction.getAuctionBitmap(null);
    }
  }

  @Override
  public void onSwitchTab(TabViews tabs, int tabIndex, boolean isSameTab) {
    if (mSliderAuctions.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED && mIsTabsInitiated)
      mSliderAuctions.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

    if (!isSameTab) {
      if (tabIndex == Auction.Type.UPCOMING.ordinal()) mCurrAuctionTypeTab = Auction.Type.UPCOMING;
      else mCurrAuctionTypeTab = Auction.Type.PAST;
      updateAuctionList();
    }
    mIsTabsInitiated = true;
  }

  @Override
  public void onPanelSlide(View panel, float slideOffset) {
    if (mBannerFullPager.getCurrentItem() != mBannerMinPager.getCurrentItem()) {
      if (slideOffset < .5f)
        mBannerMinPager.setCurrentItem(mBannerFullPager.getCurrentItem(), false);
      else mBannerFullPager.setCurrentItem(mBannerMinPager.getCurrentItem(), false);
    }

    mBannerFullView.setAlpha((1 - slideOffset) * 10f);
    mBannerFullView.setY((mBannerMinView.getMeasuredHeight() - mBannerFullView.getMeasuredHeight()) * slideOffset);
    mBannerFullView.setVisibility(slideOffset == 1 ? View.GONE : View.VISIBLE);
  }

  @Override
  public void onPanelCollapsed(View panel) {
//		mSliderAuctions.setDragView(mAuctionView);
    mSliderAuctions.setDragView(mAuctionTabs);
  }

  @Override
  public void onPanelExpanded(View panel) {
    mSliderAuctions.setDragView(mAuctionTabs);
  }

  @Override
  public void onPanelAnchored(View panel) { }

  @Override
  public void onPanelHidden(View panel) { }

  @Override
  public void onRightActionButtonClicked(BaseActionBar.ActionButton which) {
    DebugLog.d(this, which.name());
    switch (which) {
      case BUTTON_RIGHT:
        pushFragment(new SearchFragment());
        break;
      case BUTTON_RIGHT_2:
        ((MainActivity) getActivity()).setMenuFragment(MainActivity.Menu.CALENDAR, new CalendarFragment(), true);
        break;
    }
  }

  @Override
  public void onPageClick(ViewGroup parent, View view, int position) {
    try {
      Banner banner = mBannerList.get(position);
      if (banner.redirectType == Banner.Type.LINK) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(banner.link));
        startActivity(intent);
      } else pushFragment(new AuctionFragment(banner.auction));
    } catch (Exception e) {
      DebugLog.eStackTrace(e);
    }
  }

  @Override
  public void onPageSelected(int position) {
    if (!mBannerList.isEmpty()) {
      for (int i = 0; i < mBannerList.size(); i++) {
        mBannerIndicatorsFullView.getChildAt(i).setEnabled(false);
        mBannerIndicatorsMinView.getChildAt(i).setEnabled(false);
      }
      mBannerIndicatorsFullView.getChildAt(position).setEnabled(true);
      mBannerIndicatorsMinView.getChildAt(position).setEnabled(true);
    }
  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }


  @Override
  public void onPageScrollStateChanged(int state) { }

  @Override
  public void onGlobalLayout() {
    if (mMainView.getMeasuredHeight() != 0) {
      mSliderAuctions.setPanelHeight(mMainView.getMeasuredHeight() - mBannerFullView.getMeasuredHeight());
      mMainView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    pushFragment(new AuctionFragment(mAuctionList.get(position)));
  }

  @Override
  public void onResume() {
    super.onResume();
    initContents();
    getBaseFragmentActivity().registerLifecycleObserver(mLifecycleObserver);
    if (mAuctionListAdapter != null) mAuctionListAdapter.notifyDataSetChanged();
  }

  @Override
  public void onPause() {
    super.onPause();
    getBaseFragmentActivity().unregisterLifecycleObserver(mLifecycleObserver);
  }

  private BaseFragmentActivity.LifecycleObserver mLifecycleObserver = new BaseFragmentActivity.LifecycleObserver() {
    @Override
    public void onResume() {}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}

    @Override
    public void onNewIntent(Intent intent) {}

    @Override
    public void onSaveInstanceState(Bundle outState) {}

    @Override
    public void onStop() {}

    @Override
    public void onDestroy() {}

    @Override
    public boolean onBackPressed() {
      if (mSliderAuctions.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
        mSliderAuctions.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        return true;
      }
      return false;
    }

    @Override
    public boolean onBackStackChanged(int backStackCount, FragmentManager.BackStackEntry topEntry) { return false; }
  };

}
