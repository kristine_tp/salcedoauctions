package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.salcedoauctions.R;

/**
 * Created by Kristine Peromingan on 6/10/2015.
 */
public class RegisterPersonalFragment extends BaseActionBarFragment implements View.OnClickListener {

	private View mMainView;
	private TextView mProgressTxt;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initActionBar(inflater);
		if(mMainView == null) {
			mMainView = inflater.inflate(R.layout.fragment_register_personal, container, false);
			initViews();
		}
		return mMainView;
	}

	private void initActionBar(LayoutInflater inflater) {
		getBaseActionBar().setTitleText("Registration");
		mProgressTxt = (TextView) inflater.inflate(R.layout.view_registration_actionbar_progress, null);
		mProgressTxt.setText("2/3");
		getBaseActionBar().getView().addView(mProgressTxt, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
	}

	private void initViews() {
		mMainView.findViewById(R.id.input_middle).setNextFocusDownId(R.id.input_lastname);
		mMainView.findViewById(R.id.input_lastname).setNextFocusDownId(R.id.input_contact);
		mMainView.findViewById(R.id.input_district).setNextFocusDownId(R.id.input_city);
		mMainView.findViewById(R.id.input_city).setNextFocusDownId(R.id.input_postal);
		mMainView.findViewById(R.id.input_postal).setNextFocusDownId(R.id.input_country);
		mMainView.findViewById(R.id.btn_continue).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_continue) pushFragment(new RegisterPaymentFragment());
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		getBaseActionBar().getView().removeView(mProgressTxt);
	}

}
