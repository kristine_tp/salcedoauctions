package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.utils.DateHelper;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Video;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class VideoListAdapter extends ArrayAdapter<Video> implements BitmapLoader.OnLoadBitmapListener {

    private Context mContext;
    private int mLayoutResource;
    private View mContainerView;
    private OnClickVideoItemListener mOnClickVideoItemListener;

    public VideoListAdapter(Context context, int layoutResource, List<Video> videos, OnClickVideoItemListener listener) {
        super(context, layoutResource, videos);
        mContext = context;
        mLayoutResource = layoutResource;
        mOnClickVideoItemListener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderItem viewHolder;
        if (convertView == null) {
            mContainerView = parent;
            convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
            viewHolder = new ViewHolderItem();
            viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
            viewHolder.dateTxt = (TextView) convertView.findViewById(R.id.txt_date);
            viewHolder.thumbImg = (ImageView) convertView.findViewById(R.id.img_thumb);
            convertView.setTag(viewHolder);
        } else viewHolder = (ViewHolderItem) convertView.getTag();

        final Video item = getItem(position);
        viewHolder.titleTxt.setText(item.title);
        viewHolder.dateTxt.setText(DateHelper.getFormattedDate(item.date, "MMM d, yyyy"));

        if (item.image != null) viewHolder.thumbImg.setImageBitmap(item.image);
        else {
            viewHolder.thumbImg.setImageDrawable(null);
            item.getVideoBitmap(this);
        }

        if (mOnClickVideoItemListener != null) {
            viewHolder.thumbImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnClickVideoItemListener.onClickVideoItem(item);
                }
            });
        }
        return convertView;
    }

    @Override
    public void onLoadBitmap(Bitmap image) {
        if (image != null) {
            mContainerView.post(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    }

    static class ViewHolderItem {
        TextView titleTxt, dateTxt;
        ImageView thumbImg;
    }

    public interface OnClickVideoItemListener {
        void onClickVideoItem(Video video);
    }
}