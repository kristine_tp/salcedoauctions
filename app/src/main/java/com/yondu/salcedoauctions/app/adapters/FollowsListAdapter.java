package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.base.utils.DateHelper;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.app.fragments.AuctionFragment;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class FollowsListAdapter extends ArrayAdapter<Auction> {

  private Context mContext;
  private int mLayoutResource;
  private View mContainerView;

  public FollowsListAdapter(Context context, int layoutResource, List<Auction> auctions) {
    super(context, layoutResource, auctions);
    mContext = context;
    mLayoutResource = layoutResource;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    final ViewHolderItem viewHolder;
    if (convertView == null) {
      mContainerView = parent;
      convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
      viewHolder = new ViewHolderItem();
      viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
      viewHolder.categoryTxt = (TextView) convertView.findViewById(R.id.txt_category);
      viewHolder.dateTxt = (TextView) convertView.findViewById(R.id.txt_date);
      convertView.setTag(viewHolder);
    } else viewHolder = (ViewHolderItem) convertView.getTag();

    final Auction item = getItem(position);
    viewHolder.titleTxt.setText(item.title);
    viewHolder.categoryTxt.setText(item.code);
    if (item.getAuctionType() == Auction.Type.PAST)
      viewHolder.dateTxt.setText(DateHelper.getFormattedDate(item.date, "MMM d, yyyy"));
    else {
      long remainingMillis = item.getRemainingMillis();
      int days = (int) (remainingMillis / AuctionFragment.DAY_MILLIS);
      int hours = (int)((remainingMillis % AuctionFragment.DAY_MILLIS) / AuctionFragment.HOUR_MILLIS);
      int mins = (int)((remainingMillis % AuctionFragment.HOUR_MILLIS) / AuctionFragment.MINUTE_MILLIS);
      int secs = (int)((remainingMillis % AuctionFragment.MINUTE_MILLIS) / AuctionFragment.SEC_MILLIS);
      viewHolder.dateTxt.setText(days +  "d " + hours + "h " + mins + "m " + secs + "s left until bidding starts");
    }
    return convertView;
  }

  static class ViewHolderItem {
    TextView titleTxt, categoryTxt, dateTxt;
  }
}