package com.yondu.salcedoauctions.app.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.BaseFragmentActivity;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.base.utils.DateHelper;
import com.yondu.base.utils.DebugLog;
import com.yondu.base.views.TabViews;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.adapters.LotListAdapter;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Lot;
import com.yondu.salcedoauctions.model.User;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/6/2015.
 */
public class AuctionFragment extends BaseActionBarFragment implements BitmapLoader.OnLoadBitmapListener, SalcedoAuctions.OnGetLotsListener,
    SalcedoAuctions.OnFollowUpdateListener, SalcedoAuctions.OnFollowCheckListener,
    TabViews.OnTabSwitchListener, AdapterView.OnItemClickListener, View.OnClickListener {

  public static long DAY_MILLIS = 86400000;
  public static long HOUR_MILLIS = 3600000;
  public static long MINUTE_MILLIS = 60000;
  public static long SEC_MILLIS = 1000;

  private boolean mIsTabsInitiated;
  private View mDetailsView;
  private ViewGroup mDetailsContainerView;
  private TextView mCountdownTxt;
  private SlidingUpPanelLayout mSliderView;
  private TabViews mContentTabs;
  private ListView mLotsListView;
  private LotListAdapter mLotListAdapter;
  private List<Lot> mLotList = new ArrayList<Lot>();
  private SimpleAlertFragmentDialog mLoginRequiredDialog;
  private Auction mAuction;
  private SalcedoAuctions mApi;
  private User mUser;
  private AuctionCountdownTimer mCountdownTimer;

  public AuctionFragment() { }

  @SuppressLint ("ValidFragment")
  public AuctionFragment(Auction auction) {
    mAuction = auction;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if (mAuction != null) {
      getBaseActionBar().setTitleText("Auction Details");
//      getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_share);
      getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

      if (mSliderView == null) {
        mUser = User.getInstance(getContext());
        mApi = SalcedoAuctions.getInstance(getContext());
        mSliderView = (SlidingUpPanelLayout) inflater.inflate(R.layout.fragment_auction, container, false);
        initViews();
        initContents();
      }
    } else this.backToPreviousFragment();
    return mSliderView;
  }

  private void updateActionBarView() {
    getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, mUser.isLoggedIn() && mAuction.isFollowed ? R.drawable.ic_star_gold : R.drawable.ic_star);
  }

  private void initViews() {
    mContentTabs = (TabViews) mSliderView.findViewById(R.id.tab_contents);
    mDetailsContainerView = (ViewGroup) mSliderView.findViewById(R.id.grp_details_container);
    mCountdownTxt = (TextView) mSliderView.findViewById(R.id.txt_countdown);

    mContentTabs.setOnTabSwitchListener(this);
//    mSliderView.findViewById(R.id.btn_join).setOnClickListener(this);
//    mSliderView.findViewById(R.id.btn_catalogue).setOnClickListener(this);
    mSliderView.setDragView(mContentTabs);
  }

  private void initContents() {
    mApi.getLots(mAuction.id, getWeakReferenceActivity(), this);

    ((TextView) mSliderView.findViewById(R.id.txt_title)).setText(mAuction.title);
    ((TextView) mSliderView.findViewById(R.id.txt_category)).setText(mAuction.code);
    ((TextView) mSliderView.findViewById(R.id.txt_date)).setText(DateHelper.getFormattedDate(mAuction.date, "MMM d, yyyy EEEE"));
    ((TextView) mSliderView.findViewById(R.id.txt_time)).setText(DateHelper.getFormattedDate(mAuction.date, "h:mm a"));
    ((TextView) mSliderView.findViewById(R.id.txt_address)).setText(mAuction.address);

    String auctionTypeText = null;
    switch (mAuction.getAuctionType()) {
      case CURRENT:
        auctionTypeText = "Ongoing";
        mSliderView.findViewById(R.id.btn_watch).setEnabled(true);
        mSliderView.findViewById(R.id.btn_watch).setOnClickListener(this);
        break;
      case UPCOMING:
        auctionTypeText = "Upcoming";
        mSliderView.findViewById(R.id.btn_watch).setEnabled(false);
        break;
      case PAST:
        auctionTypeText = "Ended";
        mSliderView.findViewById(R.id.btn_watch).setVisibility(View.GONE);
        break;
    }
    ((TextView) mSliderView.findViewById(R.id.txt_status)).setText(auctionTypeText);

    if (mAuction.image != null)
      ((ImageView) mSliderView.findViewById(R.id.img_auction)).setImageBitmap(mAuction.image);
    else mAuction.getAuctionBitmap(this);

    initDetailsView();
    initLotsView();
  }

  private void initDetailsView() {
    mDetailsView = LayoutInflater.from(getContext()).inflate(R.layout.view_auction_details, null);
    ((TextView) mDetailsView.findViewById(R.id.txt_description)).setText(Html.fromHtml(mAuction.description));
//    ((TextView) mDetailsView.findViewById(R.id.txt_description)).setText(mAuction.description);
    ((TextView) mDetailsView.findViewById(R.id.txt_date_preview)).setText(DateHelper.getFormattedDate(mAuction.previewDate, "MMM d, yyyy\nEEEE"));
    ((TextView) mDetailsView.findViewById(R.id.txt_time_preview)).setText(DateHelper.getFormattedDate(mAuction.previewDate, "h:mm a"));
    ((TextView) mDetailsView.findViewById(R.id.txt_address_preview)).setText(mAuction.address);
  }

  private void initLotsView() {
    mLotsListView = (ListView) LayoutInflater.from(getContext()).inflate(R.layout.view_auction_lots, null);
    mLotListAdapter = new LotListAdapter(getContext(), R.layout.view_auction_lots_item, mLotList);
    mLotsListView.setAdapter(mLotListAdapter);
    mLotsListView.setOnItemClickListener(this);
  }

  private void updateFollowStatus() {
    if (mUser.isLoggedIn())
      mApi.followCheck(mUser.id, mAuction.id, getWeakReferenceActivity(), this);
  }

  private void updateCountdownView() {
    DebugLog.d(this, "date " + mAuction.date);
    if (mAuction.date.before(Calendar.getInstance())) mCountdownTxt.setVisibility(View.GONE);
    else {
      mCountdownTxt.setVisibility(View.VISIBLE);
      mCountdownTimer = new AuctionCountdownTimer((mAuction.date.getTimeInMillis() - Calendar.getInstance().getTimeInMillis()), 1000);
      mCountdownTimer.start();
    }
  }

  private void showLoginFirstDialog(String loginActionMessage) {
    View loginRequiredView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_login_required_dialog, null);
    ((TextView) loginRequiredView.findViewById(R.id.txt_message)).setText("You need to be signed in to " + loginActionMessage);
    loginRequiredView.findViewById(R.id.btn_close).setOnClickListener(this);
    loginRequiredView.findViewById(R.id.btn_login).setOnClickListener(this);
    mLoginRequiredDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
    mLoginRequiredDialog.setCustomView(loginRequiredView);
    mLoginRequiredDialog.setCancelable(false);
    mLoginRequiredDialog.show(getFragmentManager(), null);
  }

//  private void showJoinTermsDialog() {
//    View joinTermsView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_auction_join_terms, null);
//    joinTermsView.findViewById(R.id.btn_close).setOnClickListener(this);
//    mJoinTermsDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
//    mJoinTermsDialog.setCustomView(joinTermsView);
//    mJoinTermsDialog.setCancelable(false);
//    mJoinTermsDialog.show(getFragmentManager(), null);
//  }
//
//  private void showOrderCatalogueDialog() {
//    View orderCatalogueView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_auction_catalogue, null);
//    orderCatalogueView.findViewById(R.id.btn_close).setOnClickListener(this);
//    mOrderCatalogueDialog = new SimpleAlertFragmentDialog(getContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
//    mOrderCatalogueDialog.setCustomView(orderCatalogueView);
//    mOrderCatalogueDialog.setCancelable(false);
//    mOrderCatalogueDialog.show(getFragmentManager(), null);
//  }

  private void closeDialogs() {
    if (mLoginRequiredDialog != null) mLoginRequiredDialog.dismiss();
  }

  @Override
  public void onGetLots(boolean isSuccess, List<Lot> lots) {
    if (isSuccess && lots != null) {
//      ((TextView) mSliderView.findViewById(R.id.tab_lots)).setText(lots.size() + " Lots");
      mLotList.clear();
      mLotList.addAll(lots);
      mLotListAdapter.notifyDataSetChanged();
      for (Lot lot : lots) lot.getLotBitmap(null);
    }
  }

  @Override
  public void onFollowUpdate(boolean isSuccess) {
    if (isSuccess) {
      mAuction.isFollowed = !mAuction.isFollowed;
      updateActionBarView();
    }
  }

  @Override
  public void onFollowCheck(boolean isSuccess, boolean isFollowed) {
    mAuction.isFollowed = isFollowed;
    updateActionBarView();
  }

  @Override
  public void onSwitchTab(TabViews tabs, int tabIndex, boolean isSameTab) {
    if (mSliderView.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED && mIsTabsInitiated)
      mSliderView.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

    if (!isSameTab) {
      int tabId = tabs.getChildAt(tabIndex).getId();
      View content;
      if (tabId == R.id.tab_details) {
        if (mDetailsView == null) initDetailsView();
        content = mDetailsView;
      } else {
        if (mLotsListView == null) initLotsView();
        content = mLotsListView;
      }
      mDetailsContainerView.removeAllViews();
      mDetailsContainerView.addView(content);
    }
    mIsTabsInitiated = true;
  }

  @Override
  public void onRightActionButtonClicked(BaseActionBar.ActionButton which) {
    switch (which) {
      case BUTTON_RIGHT:
        if (mUser.isLoggedIn())
          mApi.followUpdate(!mAuction.isFollowed, mUser.id, mAuction.id, getWeakReferenceActivity(), this);
        else showLoginFirstDialog("follow Auctions.");
        break;
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    pushFragment(new LotFragment(mLotList.get(position)));
  }

  @Override
  public void onClick(View view) {
    if (view.getId() == R.id.btn_close) closeDialogs();
    else if (view.getId() == R.id.btn_login) {
      closeDialogs();
      pushFragment(new LoginFragment());
    } else if (view.getId() == R.id.btn_watch) {
      if(mUser.isLoggedIn()) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(mAuction.videoLink));
        startActivity(intent);
      } else showLoginFirstDialog("watch Auction Live Streams.");
    }
  }

  @Override
  public void onLoadBitmap(Bitmap image) {
    if (image != null) {
      mSliderView.post(new Runnable() {
        @Override
        public void run() {
          ((ImageView) mSliderView.findViewById(R.id.img_auction)).setImageBitmap(mAuction.image);
        }
      });
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    getBaseFragmentActivity().registerLifecycleObserver(mLifecycleObserver);
    if (mLotListAdapter != null) mLotListAdapter.notifyDataSetChanged();
    updateCountdownView();
    updateActionBarView();
    updateFollowStatus();
  }

  @Override
  public void onPause() {
    super.onPause();
    getBaseFragmentActivity().unregisterLifecycleObserver(mLifecycleObserver);
    if (mCountdownTimer != null) mCountdownTimer.cancel();
  }

  private BaseFragmentActivity.LifecycleObserver mLifecycleObserver = new BaseFragmentActivity.LifecycleObserver() {
    @Override
    public void onResume() {}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {}

    @Override
    public void onNewIntent(Intent intent) {}

    @Override
    public void onSaveInstanceState(Bundle outState) {}

    @Override
    public void onStop() {}

    @Override
    public void onDestroy() {}

    @Override
    public boolean onBackPressed() {
      if (mSliderView.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
        mSliderView.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        return true;
      }
      return false;
    }

    @Override
    public boolean onBackStackChanged(int backStackCount, FragmentManager.BackStackEntry topEntry) { return false; }
  };

  public class AuctionCountdownTimer extends CountDownTimer {
    public AuctionCountdownTimer(long startTime, long interval) {
      super(startTime, interval);
    }

    @Override
    public void onTick(long millisUntilFinished) {
      int days = (int) (millisUntilFinished / DAY_MILLIS);
      int hours = (int) ((millisUntilFinished % DAY_MILLIS) / HOUR_MILLIS);
      int mins = (int) ((millisUntilFinished % HOUR_MILLIS) / MINUTE_MILLIS);
      int secs = (int) ((millisUntilFinished % MINUTE_MILLIS) / SEC_MILLIS);
      mCountdownTxt.setText(days + "d " + hours + "h " + mins + "m " + secs + "s left until bidding starts");
    }

    @Override
    public void onFinish() {
      updateCountdownView();
    }
  }
}
