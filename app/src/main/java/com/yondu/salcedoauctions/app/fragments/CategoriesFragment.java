package com.yondu.salcedoauctions.app.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.adapters.CategoryListAdapter;
import com.yondu.salcedoauctions.model.Category;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/29/2015.
 */
public class CategoriesFragment extends CommonMenuFragment implements SalcedoAuctions.OnGetCategoriesListener, BitmapLoader.OnLoadBitmapListener,
		AdapterView.OnItemClickListener {

	private ListView mCategoriesListView;
	private CategoryListAdapter mCategoryListAdapter;
	private List<Category> mCategoryList = new ArrayList<Category>();
	private SalcedoAuctions mApi;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		getBaseActionBar().setTitleText("Categories");
		getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_search);
		getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

		if(mCategoriesListView == null) {
			mApi = SalcedoAuctions.getInstance(getContext());
			mCategoriesListView = (ListView) inflater.inflate(R.layout.fragment_categories, container, false);
			initViews();
			mApi.getCategories(getWeakReferenceActivity(), this);
		}
		return mCategoriesListView;
	}

	private void initViews() {
		mCategoryListAdapter = new CategoryListAdapter(getContext(), R.layout.view_category_item, mCategoryList);
		mCategoriesListView.setAdapter(mCategoryListAdapter);
		mCategoriesListView.setOnItemClickListener(this);
	}

	@Override
	public void onGetCategories(boolean isSuccess, List<Category> categories) {
		if(isSuccess && categories != null) {
			mCategoryList.clear();
			for(int i=0; i<categories.size(); i++) {
				mCategoryList.add(categories.get(i));
				mCategoryList.get(i).getCategoryBitmap(this);
			}
			mCategoryListAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onRightActionButtonClicked(BaseActionBar.ActionButton which) {
		DebugLog.d(this, which.name());
		switch(which) {
			case BUTTON_RIGHT:
				pushFragment(new SearchFragment());
				break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		for(Category category : mCategoryList) category.interruptGetCategoryBitmap();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		pushFragment(new CategoryAuctionsFragment(mCategoryList.get(position)));
	}

	@Override
	public void onLoadBitmap(Bitmap image) {
		mCategoriesListView.post(new Runnable() {
			@Override
			public void run() {
				mCategoryListAdapter.notifyDataSetChanged();
			}
		});
	}
}
