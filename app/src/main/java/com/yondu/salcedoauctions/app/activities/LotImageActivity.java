package com.yondu.salcedoauctions.app.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.polites.android.GestureImageView;
import com.yondu.salcedoauctions.R;

public class LotImageActivity extends AppCompatActivity implements View.OnClickListener {

	public static final String EXTRA_IMAGE = "image";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lot_image);

		byte[] byteArray = getIntent().getByteArrayExtra(EXTRA_IMAGE);
		Bitmap image = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
		((GestureImageView) findViewById(R.id.img_lot)).setImageBitmap(image);
		findViewById(R.id.btn_collapse).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		finish();
	}

}
