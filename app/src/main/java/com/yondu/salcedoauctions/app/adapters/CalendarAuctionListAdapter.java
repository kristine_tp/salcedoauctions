package com.yondu.salcedoauctions.app.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.util.BitmapLoader;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Kristine Peromingan on 7/6/2015.
 */
public class CalendarAuctionListAdapter extends ArrayAdapter<Auction> implements BitmapLoader.OnLoadBitmapListener {

	private Context mContext;
	private int mLayoutResource;
	private View mContainerView;

	public CalendarAuctionListAdapter(Context context, int layoutResource, List<Auction> auctions) {
		super(context, layoutResource, auctions);
		mContext = context;
		mLayoutResource = layoutResource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolderItem viewHolder;
		if(convertView == null) {
			mContainerView = parent;
			convertView = LayoutInflater.from(mContext).inflate(mLayoutResource, null);
			viewHolder = new ViewHolderItem();
			viewHolder.dateTxt = (TextView) convertView.findViewById(R.id.txt_date);
			viewHolder.codeTxt = (TextView) convertView.findViewById(R.id.txt_code);
			viewHolder.titleTxt = (TextView) convertView.findViewById(R.id.txt_title);
			viewHolder.addressTxt = (TextView) convertView.findViewById(R.id.txt_address);
			viewHolder.thumbImg = (ImageView) convertView.findViewById(R.id.img_thumb);
			convertView.setTag(viewHolder);
		} else viewHolder = (ViewHolderItem) convertView.getTag();

		final Auction item = getItem(position);
		viewHolder.dateTxt.setText(String.valueOf(item.date.get(Calendar.DAY_OF_MONTH)));
		viewHolder.codeTxt.setText(item.code);
		viewHolder.titleTxt.setText(item.title);
		viewHolder.addressTxt.setText(item.address);

		if(item.image != null) viewHolder.thumbImg.setImageBitmap(item.image);
		else {
			viewHolder.thumbImg.setImageBitmap(null);
			item.getAuctionBitmap(this);
		}
		return convertView;
	}

	@Override
	public void onLoadBitmap(Bitmap image) {
		if(image != null) {
			mContainerView.post(new Runnable() {
				@Override
				public void run() {
					notifyDataSetChanged();
				}
			});
		}
	}

	static class ViewHolderItem {
		TextView dateTxt, codeTxt, titleTxt, addressTxt;
		ImageView thumbImg;
	}
}