package com.yondu.salcedoauctions.app.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarActivity;
import com.yondu.base.app.BaseActivityFragment;
import com.yondu.base.utils.Connectivity;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.activities.MainActivity;
import com.yondu.salcedoauctions.app.activities.RegisterActivity;
import com.yondu.salcedoauctions.model.User;

/**
 * Created by Kristine Peromingan on 6/10/2015.
 */
public class LoginFragment extends BaseActivityFragment implements SalcedoAuctions.OnLoginListener, View.OnKeyListener, View.OnClickListener {

  private boolean mIsInitialLogin;
  private View mMainView;
  private EditText mEmailInput, mPasswordInput;
  private android.view.animation.Animation mInputErrorAnim;
  private SalcedoAuctions mApi;
  private User mUser;

  public LoginFragment() {}

  @SuppressLint ("ValidFragment")
  public LoginFragment(boolean isInitialLogin) {
    mIsInitialLogin = isInitialLogin;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    initActionBar();
    if (mMainView == null) {
      mUser = User.getInstance(getContext());
      mApi = SalcedoAuctions.getInstance(getContext());
      mMainView = inflater.inflate(R.layout.fragment_login, container, false);
      initViews();
    }
    return mMainView;
  }

  private void initActionBar() {
    try {
      BaseActionBar actionBar = ((BaseActionBarActivity) getActivity()).getBaseActionBar();
      actionBar.setTitleText("Sign In");
      actionBar.hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
      actionBar.hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);
    } catch (Exception e) {DebugLog.eStackTrace(e);}
  }

  private void initViews() {
    mEmailInput = (EditText) mMainView.findViewById(R.id.input_email);
    mPasswordInput = (EditText) mMainView.findViewById(R.id.input_password);
    mInputErrorAnim = AnimationUtils.loadAnimation(getContext(), R.anim.input_error_shake);

    mPasswordInput.setOnKeyListener(this);
    mMainView.findViewById(R.id.btn_forgot).setOnClickListener(this);
    mMainView.findViewById(R.id.btn_login).setOnClickListener(this);
    mMainView.findViewById(R.id.btn_register).setOnClickListener(this);

    if (mIsInitialLogin) mMainView.findViewById(R.id.btn_skip).setOnClickListener(this);
    else mMainView.findViewById(R.id.btn_skip).setVisibility(View.GONE);
  }

  private boolean isInputValid() {
    boolean isValid = true;
    long delay = 0;

    if (!Patterns.EMAIL_ADDRESS.matcher(mEmailInput.getText()).matches()) {
      mEmailInput.startAnimation(mInputErrorAnim);
      delay = 500;
      isValid = false;
    }
    if (mPasswordInput.getText().length() == 0) {
      mPasswordInput.postDelayed(new Runnable() {
        @Override
        public void run() {
          mPasswordInput.startAnimation(mInputErrorAnim);
        }
      }, delay);
      isValid = false;
    }
    return isValid;
  }

  private void onClickLogin() {
    if (isInputValid() && Connectivity.isConnected(getWeakReferenceActivity(), true)) {
      showLoading(true);
      mApi.login(mEmailInput.getText().toString(), mPasswordInput.getText().toString(), getWeakReferenceActivity(), this);
    }
  }

  @Override
  public void onLogin(boolean isSuccess, User user, String errorMsg) {
    if (isSuccess && user != null) {
      mUser.login(user.id, user.email, user.firstname, user.lastname, user.salutation);
      getActivity().onBackPressed();
    } else {
      showLoading(false);
      Toast toast = Toast.makeText(getContext(), errorMsg, Toast.LENGTH_LONG);
      toast.getView().setBackgroundColor(getResources().getColor(R.color.red));
      toast.setGravity(Gravity.CENTER, 0, 0);
      toast.show();
    }
  }

  @Override
  public boolean onKey(View view, int keyCode, KeyEvent event) {
    if (view.getId() == R.id.input_password) {
      if (keyCode == EditorInfo.IME_ACTION_DONE || (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
        onClickLogin();
        return true;
      }
    }
    return false;
  }

  @Override
  public void onClick(View view) {
    if (view.getId() == R.id.btn_login) onClickLogin();
    if (view.getId() == R.id.btn_skip) {
      startActivity(new Intent(getContext(), MainActivity.class));
      getActivity().finish();
    } else if (view.getId() == R.id.btn_register) {
      if (mIsInitialLogin) startActivity(new Intent(getContext(), RegisterActivity.class));
      else pushFragment(new RegisterFragment());
    } else if (view.getId() == R.id.btn_forgot) {
      try {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(SalcedoAuctions.WEB_REGISTER));
        startActivity(intent);
      } catch(Exception e) {
        DebugLog.eStackTrace(e);
      }

    }
  }

  @Override
  public void onDestroy() {
    mApi.cancelRequest(SalcedoAuctions.REQUEST_LOGIN);
    super.onDestroy();
    showLoading(false);
  }
}
