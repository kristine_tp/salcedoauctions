package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.salcedoauctions.R;

/**
 * Created by Kristine Peromingan on 6/10/2015.
 */
public class RegisterPaymentFragment extends BaseActionBarFragment implements View.OnClickListener {

	private View mMainView;
	private TextView mProgressTxt;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		initActionBar(inflater);
		if(mMainView == null) {
			mMainView = inflater.inflate(R.layout.fragment_register_payment, container, false);
			initViews();
		}
		return mMainView;
	}

	private void initActionBar(LayoutInflater inflater) {
		getBaseActionBar().setTitleText("Registration");
		mProgressTxt = (TextView) inflater.inflate(R.layout.view_registration_actionbar_progress, null);
		mProgressTxt.setText("3/3");
		getBaseActionBar().getView().addView(mProgressTxt, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
	}

	private void initViews() {
		mMainView.findViewById(R.id.input_card).setNextFocusDownId(R.id.input_card_1);
		mMainView.findViewById(R.id.input_card_1).setNextFocusDownId(R.id.input_card_2);
		mMainView.findViewById(R.id.input_card_2).setNextFocusDownId(R.id.input_card_3);
		mMainView.findViewById(R.id.input_card_3).setNextFocusDownId(R.id.input_card_4);
		mMainView.findViewById(R.id.btn_submit).setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.btn_submit) backToFirstFragment();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		getBaseActionBar().getView().removeView(mProgressTxt);
	}
}
