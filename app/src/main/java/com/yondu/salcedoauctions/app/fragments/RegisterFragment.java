package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.utils.AppUtils;
import com.yondu.base.utils.Connectivity;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;

/**
 * Created by Kristine Peromingan on 6/9/2015.
 */
public class RegisterFragment extends BaseActionBarFragment implements SalcedoAuctions.OnRegisterListener, View.OnKeyListener, View.OnClickListener {

  private View mMainView;
  private EditText mFistnameInput, mLastnameInput, mEmailInput, mPasswordInput;
  private Animation mInputErrorAnim;
  private SalcedoAuctions mApi;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    getBaseActionBar().setTitleText("Registration");
    getBaseActionBar().setHomeAction(BaseActionBar.ActionHome.BACK, R.drawable.ic_back);
    if (mMainView == null) {
      mApi = SalcedoAuctions.getInstance(getContext());
      mMainView = inflater.inflate(R.layout.fragment_register, container, false);
      initViews();
    }
    return mMainView;
  }

  private void initViews() {
    mFistnameInput = (EditText) mMainView.findViewById(R.id.input_firstname);
    mLastnameInput = (EditText) mMainView.findViewById(R.id.input_lastname);
    mEmailInput = (EditText) mMainView.findViewById(R.id.input_email);
    mPasswordInput = (EditText) mMainView.findViewById(R.id.input_password);
    mInputErrorAnim = AnimationUtils.loadAnimation(getContext(), R.anim.input_error_shake);

    mPasswordInput.setOnKeyListener(this);
    mMainView.findViewById(R.id.btn_register).setOnClickListener(this);
  }

  private void onClickRegister() {
    if (isInputValid() && Connectivity.isConnected(getWeakReferenceActivity(), true)) {
      showLoading(true);
      mApi.register(mFistnameInput.getText().toString(), mLastnameInput.getText().toString(), mEmailInput.getText().toString(), mPasswordInput.getText().toString(), getWeakReferenceActivity(), this);
    }
  }

  private boolean isInputValid() {
    boolean isValid = true;
    long delay = 0;

    if (mFistnameInput.getText().length() == 0) {
      delay = 500;
      mFistnameInput.startAnimation(mInputErrorAnim);
      isValid = false;
    }
    if (mLastnameInput.getText().length() == 0) {
      mLastnameInput.postDelayed(new Runnable() {
        @Override
        public void run() {
          mLastnameInput.startAnimation(mInputErrorAnim);
        }
      }, delay);
      delay += 500;
      isValid = false;
    }
    if (!Patterns.EMAIL_ADDRESS.matcher(mEmailInput.getText()).matches()) {
      mEmailInput.postDelayed(new Runnable() {
        @Override
        public void run() {
          mMainView.findViewById(R.id.grp_email).startAnimation(mInputErrorAnim);
        }
      }, delay);
      delay += 500;
      isValid = false;
    }
    if (mPasswordInput.getText().length() == 0) {
      mPasswordInput.postDelayed(new Runnable() {
        @Override
        public void run() {
          mMainView.findViewById(R.id.grp_password).startAnimation(mInputErrorAnim);
        }
      }, delay);
      isValid = false;
    }
    return isValid;
  }

  @Override
  public void onRegister(boolean isSuccess, String message) {
    showLoading(false);
    Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
    if (!isSuccess) toast.getView().setBackgroundColor(getResources().getColor(R.color.red));
    toast.setGravity(Gravity.CENTER, 0, 0);
    toast.show();
    if (isSuccess) getActivity().onBackPressed();
  }

  @Override
  public boolean onKey(View view, int keyCode, KeyEvent event) {
    if (view.getId() == R.id.input_password) {
      if (keyCode == EditorInfo.IME_ACTION_DONE || (keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
        onClickRegister();
        return true;
      }
    }
    return false;
  }

  @Override
  public void onClick(View view) {
    if (view.getId() == R.id.btn_register) onClickRegister();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    showLoading(false);
    mApi.cancelRequest(SalcedoAuctions.REQUEST_LOGIN);
  }
}
