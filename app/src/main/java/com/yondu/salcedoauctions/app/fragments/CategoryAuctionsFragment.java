package com.yondu.salcedoauctions.app.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.BaseActionBarFragment;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.base.utils.DebugLog;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.adapters.AuctionListAdapter;
import com.yondu.salcedoauctions.app.adapters.FollowsListAdapter;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kristine Peromingan on 6/29/2015.
 */
public class CategoryAuctionsFragment extends BaseActionBarFragment implements SalcedoAuctions.OnGetAuctionsListener, AdapterView.OnItemClickListener {

  private ListView mAuctionListView;
  private AuctionListAdapter mAuctionListAdapter;
  private List<Auction> mAuctionList = new ArrayList<Auction>();
  private Category mCategory;
  private SalcedoAuctions mApi;

  public CategoryAuctionsFragment() {}

  @SuppressLint ("ValidFragment")
  public CategoryAuctionsFragment(Category category) {
    mCategory = category;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if (mCategory != null) {
      getBaseActionBar().setTitleText(mCategory.name);
      getBaseActionBar().showButton(BaseActionBar.ActionButton.BUTTON_RIGHT, R.drawable.ic_search);
      getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

      if (mAuctionListView == null) {
        mApi = SalcedoAuctions.getInstance(getContext());
        mAuctionListView = (ListView) inflater.inflate(R.layout.fragment_common_listview, container, false);
        initCategoryAuctions();
      }
    } else this.backToPreviousFragment();
    return mAuctionListView;
  }

  private void getCategoryAuctions() {
    if (mAuctionList.isEmpty()) mApi.getCategoryAuctions(mCategory.id, getWeakReferenceActivity(), this);
  }

  @Override
  public void onResume() {
    super.onResume();
    getCategoryAuctions();
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  private void initCategoryAuctions() {
    mAuctionListAdapter = new AuctionListAdapter(getContext(), R.layout.view_home_auctions_item, mAuctionList);
    mAuctionListView.setAdapter(mAuctionListAdapter);
    mAuctionListView.setOnItemClickListener(this);
  }

  @Override
  public void onGetAuctions(boolean isSuccess, Auction.Type auctionType, List<Auction> auctions) {
    if (isSuccess && auctions != null) {
      mAuctionList.clear();
      mAuctionList.addAll(auctions);
      mAuctionListAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    pushFragment(new AuctionFragment(mAuctionList.get(position)));
  }

  @Override
  public void onRightActionButtonClicked(BaseActionBar.ActionButton which) {
    DebugLog.d(this, which.name());
    switch(which) {
      case BUTTON_RIGHT:
        pushFragment(new SearchFragment());
        break;
    }
  }

}
