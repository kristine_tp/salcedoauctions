package com.yondu.salcedoauctions.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.base.app.SimpleActionBarActivity;
import com.yondu.base.app.SimpleAlertFragmentDialog;
import com.yondu.base.utils.DebugLog;
import com.yondu.base.views.TabViews;
import com.yondu.base.views.WrappingSlidingPaneLayout;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.app.adapters.InterestListAdapter;
import com.yondu.salcedoauctions.app.fragments.FollowedAuctionsFragment;
import com.yondu.salcedoauctions.app.fragments.CategoriesFragment;
import com.yondu.salcedoauctions.app.fragments.CommonMenuFragment;
import com.yondu.salcedoauctions.app.fragments.CommonPageFragment;
import com.yondu.salcedoauctions.app.fragments.CurrencyConverterFragment;
import com.yondu.salcedoauctions.app.fragments.HomeFragment;
import com.yondu.salcedoauctions.app.fragments.LoginFragment;
import com.yondu.salcedoauctions.app.fragments.VideosFragment;
import com.yondu.salcedoauctions.model.Auction;
import com.yondu.salcedoauctions.model.Category;
import com.yondu.salcedoauctions.model.User;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends SimpleActionBarActivity implements User.OnUserStateChangeListener, SalcedoAuctions.OnGetInterestsListener,
    SalcedoAuctions.OnGetCategoriesListener, WrappingSlidingPaneLayout.PanelSlideListener, TabViews.OnTabSwitchListener, AdapterView.OnItemClickListener, View.OnClickListener {

  public enum Menu {AUCTIONS, PAST_AUCTIONS, FOLLOWED, CATEGORIES, VIDEOS, CURRENCY, FAQS, CONTACT, ABOUT, SETTINGS, LOGOUT, LOGIN, CALENDAR}

  private Menu mCurrMenu;
  private ViewGroup mMenuView;
  private View mSliderCover;
  private TabViews mMenuTabs;
  private HomeFragment mHomeFragment;
  private User mUser;
  private SalcedoAuctions mApi;
  private ListView mCategoryListView;
  private List<Category> mCategoryList = new ArrayList<Category>();
  private InterestListAdapter mCategoryNamesAdapter;
  private SimpleAlertFragmentDialog mInterestsDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    DebugLog.setTag("salcedo");
    mHomeFragment = new HomeFragment();
    setInitialFragment(mHomeFragment);
    setActionBarNavButtons(R.drawable.ic_menu, R.drawable.ic_back);
    super.onCreate(savedInstanceState);

    getBaseActionBar().setHomeAction(BaseActionBar.ActionHome.HOME, R.drawable.ic_menu);
    mApi = SalcedoAuctions.getInstance(getApplicationContext());
    mUser = User.getInstance(getApplicationContext());
    initViews();
    if (mUser.isLoggedIn()) initUserInterests();
  }

  @Override
  protected void onBaseSetContentView() {
    setContentView(R.layout.activity_main);
    setLoadingView(R.id.main_loading);
  }

  @Override
  public void onResume() {
    super.onResume();
    updateMenuView();
    mUser.registerOnUserStateChangeListener(this);
  }

  @Override
  public void onPause() {
    super.onPause();
    mUser.unregisterOnUserStateChangeListener(this);
  }

  private void initViews() {
    mSliderCover = findViewById(R.id.slider_cover);
    initMenuView();
  }

  private void initMenuView() {
    mMenuView = (ViewGroup) LayoutInflater.from(getApplicationContext()).inflate(R.layout.view_menu, null);
    setSlidingMenu(mMenuView, 0);
    getMenuSlider().setPanelSlideListener(this);
    getMenuSlider().enableSwipe(false);

    mMenuTabs = (TabViews) mMenuView.findViewById(R.id.tabs_menu);
    mMenuTabs.setOnTabSwitchListener(this);
    mMenuView.findViewById(R.id.btn_login).setOnClickListener(this);
  }

  private void updateMenuView() {
    if (mUser.isLoggedIn()) {
      mMenuView.findViewById(R.id.menu_logout).setVisibility(View.VISIBLE);
      mMenuView.findViewById(R.id.btn_login).setVisibility(View.GONE);

      String greeting = mUser.firstname;
      if (mUser.salutation != null && !mUser.salutation.equalsIgnoreCase("null"))
        greeting = mUser.salutation + " " + mUser.lastname;
      ((TextView) mMenuView.findViewById(R.id.txt_user)).setText(greeting);
    } else {
      mMenuView.findViewById(R.id.menu_logout).setVisibility(View.GONE);
      mMenuView.findViewById(R.id.btn_login).setVisibility(View.VISIBLE);
      ((TextView) mMenuView.findViewById(R.id.txt_user)).setText("Guest");
    }
  }

  public void switchMenu(Menu menu) {
    mMenuTabs.switchTab(menu.ordinal());
  }

  public void setMenuFragment(Menu menu, Fragment fragment) {
    setMenuFragment(menu, fragment, false);
  }

  public void setMenuFragment(Menu menu, Fragment fragment, boolean removeMenuSelection) {
    if (menu != mCurrMenu) {
      if (removeMenuSelection)
        mMenuTabs.getChildAt(mMenuTabs.getCurrTab()).setSelected(false);
      mCurrMenu = menu;
      replaceStackedContentFragments(fragment);
    }
  }

  private void setMenuCommonPageFragment(Menu menu, String title, String url) {
    CommonPageFragment pageFragment = new CommonPageFragment();
    pageFragment.setPageInfo(title, url);
    setMenuFragment(menu, pageFragment, false);
  }

  private boolean isMenuFragment(Fragment fragment) {
    return CommonMenuFragment.class.isAssignableFrom(fragment.getClass());
  }

  private void initUserInterests() {
    mApi.getInterests(mUser.id, new WeakReference<Activity>(this), this);
  }

  private void submitInterests() {
    List<Integer> interests = new ArrayList<Integer>();
    for (Category category : mCategoryList) {
      if (category.isInterest) interests.add(category.id);
    }

    if (!interests.isEmpty())
      mApi.addInterests(mUser.id, interests, new WeakReference<Activity>(this), null);
  }

  private void showInterestsDialog() {
    View interestsView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_interests, null);
    interestsView.findViewById(R.id.btn_close).setOnClickListener(this);
    interestsView.findViewById(R.id.btn_ok).setOnClickListener(this);
    mCategoryListView = (ListView) interestsView.findViewById(R.id.list_interests);
    mCategoryListView.setAdapter(mCategoryNamesAdapter);
    mCategoryListView.setOnItemClickListener(this);

    mInterestsDialog = new SimpleAlertFragmentDialog(getApplicationContext(), SimpleAlertFragmentDialog.DialogStyle.PANEL, false);
    mInterestsDialog.setCustomView(interestsView);
    mInterestsDialog.setCancelable(false);
    mInterestsDialog.show(getBaseFragmentManager(), null);
  }

  private void closeDialogs() {
    if (mInterestsDialog != null) mInterestsDialog.dismiss();
  }

  @Override
  public void onGetInterests(boolean isSuccess, boolean isInitiated) {
    if (isSuccess && !isInitiated) {
      mCategoryNamesAdapter = new InterestListAdapter(getApplicationContext(), R.layout.view_interests_item, mCategoryList);
      mApi.getCategories(new WeakReference<Activity>(this), this);
    }
  }

  @Override
  public void onGetCategories(boolean isSuccess, List<Category> categories) {
    if (isSuccess && categories != null) {
      mCategoryList.clear();
      mCategoryList.addAll(categories);
    }
    showInterestsDialog();
  }

  @Override
  public void onUserStateChange(boolean isLoggedIn) {
    updateMenuView();
    if (isLoggedIn) initUserInterests();
  }

  @Override
  public void onSwitchTab(TabViews tabs, int tabIndex, boolean isSameTab) {
    int tabId = tabs.getChildAt(tabIndex).getId();
    Menu menu = Menu.values()[tabIndex];

    if (tabId == R.id.menu_auctions) {
      mCurrMenu = menu;
      backToFirstFragment();
      mHomeFragment.setInitState();
    } else if (tabId == R.id.menu_auctions_past) {
      mCurrMenu = menu;
      backToFirstFragment();
      mHomeFragment.setTab(Auction.Type.PAST);
    } else if (tabId == R.id.menu_auctions_followed)
      setMenuFragment(menu, new FollowedAuctionsFragment());
    else if (tabId == R.id.menu_categories) setMenuFragment(menu, new CategoriesFragment());
    else if (tabId == R.id.menu_videos) setMenuFragment(menu, new VideosFragment());
    else if (tabId == R.id.menu_converter)
      setMenuFragment(menu, new CurrencyConverterFragment());
    else if (tabId == R.id.menu_faq)
      setMenuCommonPageFragment(menu, "FAQs", SalcedoAuctions.PAGE_FAQ);
    else if (tabId == R.id.menu_contact)
      setMenuCommonPageFragment(menu, "Contact", SalcedoAuctions.PAGE_CONTACT);
    else if (tabId == R.id.menu_about)
      setMenuCommonPageFragment(menu, "About", SalcedoAuctions.PAGE_ABOUT);
    else if (tabId == R.id.menu_logout) {
      mUser.logout();
      mCurrMenu = Menu.AUCTIONS;
      backToFirstFragment();
      return;
    }

    getMenuSlider().closePane();
  }

  @Override
  public void onPanelSlide(View panel, float slideOffset) { }

  @Override
  public void onPanelOpened(View panel) {
    mSliderCover.setVisibility(View.VISIBLE);
    getMenuSlider().enableSwipe(true);
  }

  @Override
  public void onPanelClosed(View panel) {
    mSliderCover.setVisibility(View.GONE);
    if (getBaseFragmentManager().getBackStackEntryCount() == 0) getMenuSlider().enableSwipe(false);
  }

  @Override
  protected void onBackStackChanged(int backStackCount, FragmentManager.BackStackEntry topEntry) {
    try {
      if (topEntry != null && isMenuFragment(getBaseFragmentManager().findFragmentByTag(topEntry.getName())) && backStackCount == 1) {
        setActionHome(BaseActionBar.ActionHome.HOME);
        getMenuSlider().enableSwipe(true);
      } else {
        if (backStackCount == 0) {
          if (mCurrMenu == Menu.AUCTIONS) {
            mMenuTabs.getChildAt(mMenuTabs.getCurrTab()).setSelected(false);
            mMenuTabs.getChildAt(0).setSelected(true);
            mMenuTabs.scrollTo(0, 0);
          }
        }
        getMenuSlider().enableSwipe(false);
        super.onBackStackChanged(backStackCount, topEntry);
      }
    } catch (Exception e) {
      DebugLog.eStackTrace(e);
    }
  }

  @Override
  public void onBackPressed() {
    if (getBaseFragmentManager().getBackStackEntryCount() == 1) mCurrMenu = Menu.AUCTIONS;
    super.onBackPressed();
  }

  @Override
  public void onHomeButtonClicked(BaseActionBar.ActionHome homeMode) {
    switch (homeMode) {
      case HOME:
        getMenuSlider().toggleOpenPane();
        break;
      default:
        super.onHomeButtonClicked(homeMode);
        break;
    }
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Category category = mCategoryList.get(position);
    category.isInterest = !category.isInterest;
    ((CheckedTextView) view.findViewById(R.id.check_interest)).setChecked(category.isInterest);
  }

  @Override
  public void onClick(View view) {
    if (view.getId() == R.id.btn_login) {
      getMenuSlider().closePane();
      setMenuFragment(Menu.LOGIN, new LoginFragment(), true);
    } else if (view.getId() == R.id.btn_close) closeDialogs();
    else if (view.getId() == R.id.btn_ok) {
      closeDialogs();
      submitInterests();
    }
  }
}
