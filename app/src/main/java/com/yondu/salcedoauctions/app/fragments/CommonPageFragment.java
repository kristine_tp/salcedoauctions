package com.yondu.salcedoauctions.app.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.yondu.base.app.BaseActionBar;
import com.yondu.salcedoauctions.R;
import com.yondu.salcedoauctions.api.SalcedoAuctions;
import com.yondu.salcedoauctions.model.Page;

/**
 * Created by Kristine Peromingan on 6/29/2015.
 */
public class CommonPageFragment extends CommonMenuFragment implements SalcedoAuctions.OnGetPageListener {

  private String mTitle, mUrl;
  private WebView mWebView;
  private SalcedoAuctions mApi;

  public void setPageInfo(String title, String url) {
    mTitle = title;
    mUrl = url;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if(mTitle != null && mUrl != null) {
      getBaseActionBar().setTitleText(mTitle);
      getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT);
      getBaseActionBar().hideButton(BaseActionBar.ActionButton.BUTTON_RIGHT_2);

      if(mWebView == null) {
        mApi = SalcedoAuctions.getInstance(getContext());
        mWebView = (WebView) inflater.inflate(R.layout.fragment_common_page, container, false);
        initContent();
      }
    } else this.backToPreviousFragment();
    return mWebView;
  }

  private void initContent() {
    mApi.getPage(mUrl, getWeakReferenceActivity(), this);
  }

  @Override
  public void onGetPage(boolean isSuccess, Page page) {
    if(isSuccess && page != null) {
      String head = "<head><style>@font-face {font-family: 'primary';src: url('file://" + getActivity().getFilesDir().getAbsolutePath() + "/fonts/TradeGothicLTStd.otf');}body {font-family:'primary';font-size:0.8em;line-height:130%;letter-spacing:0.06em;}</style></head>";
      String htmlData = "<html>" + head + "<body style=\"font-family: primary\">" + page.content + "</body></html>";
      mWebView.loadData(htmlData, "text/html", "UTF-8");
//      ((TextView) mWebView.findViewById(R.id.txt_content)).setText(Html.fromHtml(page.content));
    }
  }
}
